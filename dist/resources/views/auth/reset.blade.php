<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <title>TAMPICO AIRLINES</title>
  <meta name="Description" content="Todo viaje es una aventura, vívela con TAMPICO" />
  <base href="{{ url('') }}">
  <meta name="robots" content="index, follow">

  <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="author" href="http://webtilia.com">

  <!-- FAVICONS
===============================================-->
  <link rel="shortcut icon" href="web/frontend/img/general/favicon.png" />
  <link rel="apple-touch-icon" href="web/frontend/img/general/favicon.png" />

  <!-- STYLESHEETS
===============================================-->
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
  <link rel="stylesheet" href="{{ url('/fonts/futura/futura.css') }}">
  <link rel="stylesheet" href="{{ url('/css/animate.css') }}">
  <link rel="stylesheet" href="{{ url('/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ url('/css/font-awesome.css') }}">
  <link rel="stylesheet" href="{{ url('/css/material.css') }}">
  <link rel="stylesheet" href="{{ url('/css/ng-animation.css') }}">
  <link href="{{ url('/css/angular-confirm.min.css') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{ url('/css/angular-block-ui.min.css') }}" />
  <link href="{{ url('/css/bi-style.css') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{ url('/css/password-reset.css') }}">

  <!-- SCRIPTS
===============================================-->
  <!-- IE FILES
===============================================-->
  <!--[if lt IE 9]>
  <script src="ie/html5shiv.js"></script>
  <script src="ie/respond.min.js"></script>
<![endif]-->

</head>

<body ng-app="tampicoApp" id="popupContainer">
  <div ng-controller="passwordController as ctrl">
    <div class="wrapper-principal">
      <div id="wrapper">
        <div id="centerlizer">
          <div class="lm-loader"></div>

          <div class="f4" id="section-3">

            <div class="bg_c header_cont"></div>

            <div class="container">
              <div class="st1 animate slide-right" ng-show="!login">
                <div class="head_3 wow animated zoomIn">
                  <img src="{{ url('/img/general/recupera_contrasenia.png') }}" alt="mis kilometros acumulados">
                </div>

                <div class="box_orange">

                  <div class="inner_box">

                    <h2>INGRESA TUS DATOS:</h2>

                    <!--<div class="avion wow animated zoomIn" data-wow-delay="0.4s"><img src="{{ url('/img/general/avion.png') }}" alt="avion tampico"></div>-->
                    <form class="cont_form" ng-submit="validarDatos(user, formDocumento.$valid)" role="form" method="POST" name="formDocumento" id="formDocumento" novalidate>
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <!--<input type="text" name="token" value="{{ $token }}" ng-model="user.token" style="display:none">-->

                      <input type="text" name="token" value="{{ $token }}" ng-model="user.token" ng-init="user.token='<?php echo($token) ?>'" style="display:none">

                      <div class="form-group">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="etiqueta">EMAIL</div>
                          </div>
                          <div class="col-sm-6">
                            <input type="email" class="campo2 form-control" name="email" value="{{ old('email') }}" required ng-minlength="8" ng-model="user.email">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="etiqueta">CONTRASEÑA</div>
                          </div>
                          <div class="col-sm-6">
                            <input type="password" class="campo2 form-control" name="password" ng-minlength="6" ng-model="user.password" required ng-keyup="cpassValidate()">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="etiqueta">REPITE TU CONTRASEÑA</div>
                          </div>
                          <div class="col-sm-6">
                            <input type="password" class="campo2 form-control" name="password_confirmation" ng-model="user.cpassword" required ng-minlength="6" ng-keyup="cpassValidate()">
                          </div>
                        </div>
                      </div>

                      <div ng-if="passMatchErrMsg && !formDocumento.cpassword.$invalid" class="errMsg" style="text-align:right; color:red">Las contraseñas no coinciden</div>
                      <div class="form-group text-center sect-bt">
                        <input type="image" src="{{ url('/img/general/boton_km.png') }}" alt="">
                      </div>
                      <div class="form-group text-center shadow-bx">
                        <input type="image" src="{{ url('/img/general/bottom-s.png') }}" alt="">
                      </div>
                    </form>
                    <!--<div class="cloud"><img src="{{ url('/img/general/nube_kms.png') }}" alt="nube"></div>-->
                  </div>

                </div>

              </div>

            </div>
            <div class="bg_c footer_cont"></div>

          </div>
        </div>
      </div>


      <script>
        window.jQuery || document.write("<script src='{{ url('/libs/jquery-1.11.2.js') }}'><\/script>")
      </script>
      <script type="text/javascript" src="{{ url('/libs/jquery-ui.js') }}"></script>

      <!--script src="{{ url('/libs/bootstrap.min.js') }}"></script-->

      <script src="{{ url('/libs/angularjs/1.5.5/angular.min.js') }}"></script>
      <script src="{{ url('/libs/angularjs/locale-es/angular-locale_es-es.js') }}"></script>
      <script src="{{ url('/libs/angularjs/1.5.5/angular-animate.min.js') }}"></script>
      <script src="{{ url('/libs/angularjs/1.5.5/angular-aria.min.js') }}"></script>
      <script src="{{ url('/libs/angularjs/1.5.5/angular-messages.min.js') }}"></script>
      <script src="{{ url('/libs/angularjs/1.5.5/angular-route.min.js') }}"></script>
      <script src="{{ url('/libs/angularjs/1.5.5/angular-sanitize.min.js') }}"></script>
      <script src="{{ url('/libs/angular-scroll.min.js') }}"></script>
      <script src="{{ url('/libs/angular_material/angular-material.min.js') }}"></script>
      <script src="{{ url('/libs/angular-blockUI/angular-block-ui.min.js') }}"></script>
      <script src="{{ url('/libs/angular-confirm.min.js') }}"></script>

      <script type="text/javascript">
        jQuery(window).load(function() {
          jQuery(".lm-loader").fadeOut("slow");
        })
        var URL_SITE = '{{url()}}';
      </script>

      <script src="{{ url('/js/app.js') }}"></script>
      <script src="{{ url('/js/controllers/password.js') }}"></script>
      <script src="{{ url('/js/services/codes.js') }}"></script>
    </div>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 text-left">
            <a href="" class="terminos" ng-click="showModal($event,'templateTerminos.html')">Términos y condiciones</a>
            <a href="" class="politicas" ng-click="showModal($event,'templatePoliticas.html')">Política de privacidad</a>
          </div>
          <div class="col-sm-6 text-right">
            <a href="mailto:consultas@tampicoairlines.com">consultas@tampicoairlines.com</a>
            <div class="copy">Copyright © 2017 - By <a href="https://www.webtilia.com/" target="_blank">Webtilia</a></div>
          </div>
        </div>
      </div>
    </footer>


    @include('partials/modals')
  </div>
</body>

</html>
