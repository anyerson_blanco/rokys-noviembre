<meta charset="utf-8">
<title> ROKYS'S :: Paquetes del hincha</title>
<base href="{{ url('') }}">
<meta name="description" content="¡Roky's, te premia! Por la compra de 2 pie de manzana, accede al sorteo de uno de los 60 paquetes del hincha." />
<meta property="og:type" content="website">
<meta property="og:title" content=" ROKYS'S :: Paquetes del hincha">
<meta property="og:description" content="¡Roky's, te premia! Por la compra de 2 pie de manzana, accede al sorteo de uno de los 60 paquetes del hincha.">
<meta property="og:site_name" content=" ROKYS'S :: Paquetes del hincha">
<meta property="og:url" content="httpsh://rokys.com/sorteos/">
<meta property="og:image" content="https://rokys.com/sorteos/">
<meta property="article:author" content="Webtilia">
<meta name="robots" content="index, follow">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<link rel="author" href="https://webtilia.com">
<link rel="shortcut icon" href="{{ url('/img/icons/favicon-tvs.png') }}" />
<link rel="apple-touch-icon" href="{{ url('/img/icons/favicon-tvs.png') }}" />
<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/fonts/futura/futura.css') }}">
<link rel="stylesheet" href="{{ url('/css/animate.css') }}">
<link rel="stylesheet" href="{{ url('/css/hover-min.css') }}">
<link rel="stylesheet" href="{{ url('/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ url('/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ url('/css/material.css') }}">
<link rel="stylesheet" href="{{ url('/css/ng-animation.css') }}">
<link href="{{ url('/css/angular-confirm.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ url('/css/angular-block-ui.min.css') }}" />
<link rel="stylesheet" href="{{ url('/owl/owl.carousel.min.css') }}" title="" media="screen" />
<link rel="stylesheet" href="{{ url('/owl/owl.theme.default.min.css') }}" title="" media="screen" />
<link rel="stylesheet" href="{{ url('/css/hover_ideas.css') }}" title="" media="screen" />
<link href="{{ url('/css/bi-style.css') }}" rel="stylesheet" />
<link href="{{ url('/css/nice-select.css') }}" rel="stylesheet" />


<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<script>
 var BASE_URL="{{ url('') }}";
</script>
