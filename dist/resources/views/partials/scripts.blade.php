<script>
  window.jQuery || document.write("<script src='{{ url('/libs/jquery-1.11.2.js') }}'><\/script>")
</script>
<script type="text/javascript" src="{{ url('/libs/jquery-ui.js') }}"></script>
<script src="{{ url('/libs/tether.min.js') }}"></script>
<script src="{{ url('/libs/bootstrap.min.js') }}"></script>
<script src="{{ url('/libs/wow.js') }}"></script>
<script src="{{ url('/owl/owl.carousel.min.js') }}"></script>
<script src="{{ url('/libs/jquery.fullpage.min.js') }}"></script>
<script src="{{ url('/libs/jquery.nice-select.min.js') }}"></script>
<script src="{{ url('/libs/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ url('/libs/TweenMax.min.js') }}"></script>
<script src="{{ url('/js/index.js?v=1.4') }}"></script>
<script src="{{ url('/libs/classie.js') }}"></script>
<script src="{{ url('/libs/angularjs/1.5.5/angular.min.js') }}"></script>
<script src="{{ url('/libs/angularjs/locale-es/angular-locale_es-es.js') }}"></script>
<script src="{{ url('/libs/angularjs/1.5.5/angular-animate.min.js') }}"></script>
<script src="{{ url('/libs/angularjs/1.5.5/angular-aria.min.js') }}"></script>
<script src="{{ url('/libs/angularjs/1.5.5/angular-messages.min.js') }}"></script>
<script src="{{ url('/libs/angularjs/1.5.5/angular-route.min.js') }}"></script>
<script src="{{ url('/libs/angularjs/1.5.5/angular-sanitize.min.js') }}"></script>
<script src="{{ url('/libs/angular-scroll.min.js') }}"></script>
<script src="{{ url('/libs/angular_material/angular-material.min.js') }}"></script>
<script src="{{ url('/libs/angular-blockUI/angular-block-ui.min.js') }}"></script>
<script src="{{ url('/libs/angular-confirm.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.min.js"></script>
