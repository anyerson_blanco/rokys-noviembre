<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 menu-footer">
        <a href="" class="terminos" ng-click="showModal($event,'templateTerminos.html')">Términos y condiciones</a> |
        <a href="" class="politicas" ng-click="showModal($event,'templatePoliticas.html')">Política de privacidad</a>
      </div>
      <div class="col-sm-6 copy">
        <a href="mailto:consultas@tampicoairlines.com" class="f2__links">consultas@tampicoairlines.com</a>
        <div class="copy">Copyright © 2017 - By <a href="https://www.webtilia.com/" target="_blank" class="f2__links">Webtilia</a></div>
      </div>
    </div>
  </div>
</footer>
