<script type="text/ng-template" id="templateTerminos.html">

  <md-dialog aria-label="Mango (Fruit)" class="dialog-preguntas">

    <form ng-cloak>

      <md-toolbar>
        <div class="md-toolbar-tools">
          <md-button class="md-icon-button" ng-click="cancel()" style="right: 20px;position: absolute;">
            <i class="fas fa-times"></i>
          </md-button>
        </div>
      </md-toolbar>

      <md-dialog-content class="md-dialog-content--top50">

        <div class="md-dialog-content dialog-preguntas__content">

          <h2>“POLÍTICAS DE PRIVACIDAD DE DATOS PERSONALES”</h2>
          <br />
          <p><b>GRUPO ROKYS</b> SOCIEDAD ANONIMA CERRADA (ANTES GRUPO KIYAN SAC) en adelante <b>GRUPO ROKYS</b> tiene como propósito principal informar a nuestros clientes que respetamos su derecho a la privacidad cuando usa nuestra página web y se comunica electrónicamente con nosotros. Por eso, como parte de nuestro compromiso para cumplir con sus expectativas hemos establecido una política de protección de la privacidad para los usuarios de nuestra página web. De acuerdo a las disposiciones contenidas en la <b>Ley de Protección de Datos Personales –  (Ley No. 29733) y sus normas complementarias y conexas.</b>
          La Ley de Protección de Datos Personales, su Reglamento y las normas que las modifique o reemplacen (en lo sucesivo, Normativa de protección de datos personales) establece que para efectuar el tratamiento de los datos o información de una persona es necesario su consentimiento previo, informado, expreso e inequívoco. Por tal motivo, conforme a la referida ley, en el uso de medios electrónicos, <b>GRUPO ROKYS</b>considerará que usted ha brindado adecuadamente su consentimiento cuando usted disponga en “hacer clic”, “cliquear” o “pinchar”, “dar un toque”, “touch” o “pad” u otros similares cuando se le pregunte por su aceptación a los presentes términos aplicables al tratamiento de sus datos personales. La autorización para el tratamiento de sus datos personales resulta obligatoria para la ejecución de las actividades descritas en las páginas web de <b>GRUPO ROKYS</b>y aquellas señaladas en la presente Política, y en caso de negativa, ellas no se podrán realizar.

          <b>GRUPO ROKYS</b> ha adoptado todas las medidas de seguridad de protección de los datos personales legalmente requeridos, garantizando la confidencialidad de los datos de carácter personal facilitados por los Usuarios y su tratamiento de acuerdo con la legislación vigente para evitar cualquier manipulación indebida, pérdida, destrucción o acceso no autorizado por parte de terceros a esta información empleando los medios adecuados para contrarrestarlos.
          </p>

          <strong>1.  LA INFORMACIÓN QUE RECOLECTAMOS A TRAVÉS DE NUESTROS SITIOS WEB </strong>
<p>Solo recolectamos los datos personales que usted nos desee proporcionar o que son necesarios para brindarle y mejorar nuestros productos y servicios, y para los fines que se describen en el punto 3 de la presente Política y que usted acepta libremente. Nosotros recolectamos información personal directa como nombre, D.N.I., domicilio, teléfono, correo electrónico; la que esté expresamente indicada en nuestros sitios web y redes sociales a través de formularios electrónicos u otros formatos; la que usted nos proporcione indirectamente, tal como cookies (información sobre preferencias del usuario cuando visita una página web o redes sociales tales como Facebook y Twitter), direcciones IP, conexiones y sistemas de información; todo en ello en estricto cumplimiento de la Ley de Protección de Datos Personales. Dicha información podrá ser almacenada en una base de datos de titularidad de <b>GRUPO ROKYS</b>.</p>

<strong>2.	GUARDAMOS SU INFORMACIÓN DE MANERA SEGURA</strong>
<p><b>GRUPO ROKYS</b> podrá obtener información sobre el Titular de los Datos Personales, por cualquier medio permitido por la Ley, incluyendo mas no limitándose a la obtención de manera personal o mediante plataformas electrónicas y/o páginas web. El Titular de los Datos Personales, mediante el ingreso de sus datos en la Página Web o su entrega de manera presencial, autoriza expresamente a <b>GRUPO ROKYS</b>y/o su personal administrativo autorizado a tratar la información facilitada de conformidad con las disposiciones de la Ley de Protección de Datos Personales – Ley No. 29733, su Reglamento, normas conexas y complementarias. <b>GRUPO ROKYS</b> procesará y protegerá los datos del Titular de los Datos Personales a través de medios digitales, técnicos y/o administrativos para garantizar un óptimo nivel de seguridad.</p>

<strong>3. DE LA PARTICIPACIÓN DEL SORTEO</strong>
<p>Mecánica:<br>

•	Adquirir una TVS King en un distribuidor autorizado a nivel nacional.<br>
•	El participante (titular/dueño de la mototaxi) deberá completar los datos del formulario en la landing page www.tvstumejorgol.com por única vez, para poder participar de El Sorteo.<br>
•	El participante (titular/dueño de la mototaxi) deberá ingresar su tipo y número de documento de identidad, nombres y apellidos, número de teléfono (celular y fijo), correo electrónico, número de chasís de su vehículo (17 dígitos), número de motor de su vehículo (12 dígitos), razón social de distribuidor/nombre de la tienda, departamento, provincia y distrito de residencia.<br>
•	El participante deberá leer y aceptar los Términos y Condiciones de la promoción, activando el botón respectivo que indica: “He leído y acepto los Términos y Condiciones”.<br>
•	Cada participante que se registre en la landing page y que sea cliente que adquirió una mototaxi durante la vigencia de la promoción, obtendrá una opción para el sorteo. <br>
•	Las opciones para el sorteo no son acumulables.<br>
•	Un mismo participante sólo podrá resultar ganador una sola vez.<br>
</p>

<strong>3.	FINALIDAD DEL TRATAMIENDO DE SUS DATOS PERSONALES</strong>
<p>Podrá solicitar a sus clientes, proveedores, trabajadores, postulantes, y usuarios de esta página web (en adelante, los Titulares de los Datos Personales) datos personales con la siguiente finalidad:</p>
<p>
•	Atender consultas o reclamos y hacer seguimiento de estos.<br>
•	Para la promoción de los servicios de <b>GRUPO ROKYS</b>.<br>
•	Invitarlo a participar en nuestros concursos, entre otras actividades o eventos<br>
•	Para fines comerciales relacionados con los servicios ofrecidos <b>GRUPO ROKYS</b> y sus empresas vinculadas.<br>
•	Para invitarlos a completar nuestros encuestas y/o comentarios por el servicio brindado.<br>
•	Para la gestión y administración de  información de clientes, proveedores, trabajadores y/o postulantes.<br>
•	Por motivos de seguridad.<br>
</p>
<p>
  Queda expresamente establecido <b>GRUPO ROKYS</b> reconoce el derecho de los Titulares de los Datos Personales a manifestar su negativa respecto al uso y tratamiento de sus datos personales, cuando consideren que la información proporcionada no cumple ninguna de las funciones anteriormente descritas, o si los datos brindados no son necesarios para el establecimiento de una relación contractual. Esto sin perjuicio del derecho de revocación de consentimiento previamente otorgado por parte del Titular de los Datos Personales o bien para ejercer su derecho de oposición al uso de sus datos en términos de las disposiciones aplicables, derechos que en todo caso serán garantizados por la empresa en todo momento.
  Los Datos Personales proporcionados serán incorporados, según corresponda a las bases de datos de (i) clientes, (ii) proveedores, (iii) postulantes, trabajadores, y extrabajadores; así como podrán incorporarse, con las mismas finalidades, a otras bases de datos de Servicios <b>GRUPO ROKYS</b> y/o de otras empresas subsidiarias, filiales, asociadas, afiliadas o miembros del grupo económico al cual ésta pertenece y/o terceros con los que éstas mantengan una relación contractual.

</p>

<strong>4.	EJERCICIO DE SUS  DERECHOS COMO TITULAR DE DATOS PERSONALES</strong>
<p>
  El Titular de los Datos Personales es responsable de la exactitud, veracidad y autenticidad de los datos que proporcione. Los Titulares de los Datos Personales tendrán derecho de acceder a sus datos personales que hubieren sido recabados, efectuar correcciones a los mismos en caso de ser estos inexactos o incompletos, solicitar su eliminación cuando consideren que no son necesarios para alguna de las finalidades señaladas en la Política de Privacidad cuando esta sea legalmente procedente, y; a oponerse al uso o tratamiento de sus datos, en términos de la Ley y las disposiciones aplicables.
  El Titular de los Datos Personales puede ejercer sus derechos de acceso, información, rectificación, cancelación, oposición y revocación al uso de sus datos personales dirigiendo su solicitud con los requisitos solicitados en la Ley y demás normas aplicables o a la siguiente dirección electrónica: servicioalcliente@rokys.com

</p>

<strong>5.	ACCESO Y TRANSFERENCIA </strong>
<p>
<b>GRUPO ROKYS</b> nunca compartirá sus datos personales con terceros para una finalidad distinta a la que usted ha consentido. Sin embargo, también puede compartir, por encargo, sus datos personales con terceros los cuales pueden estar ubicados dentro y fuera del territorio nacional, pero solo bajo circunstancias estrictamente limitadas como las que se indican a continuación: Podemos proporcionar sus datos personales a terceros (como nuestros proveedores de servicios de Internet, administradores de páginas web y managers de cuentas en redes sociales, call centers, servicios de mensajería, transportes, etc.) que nos ayudan a mantener y administrar este u otros sitios (incluyendo pero no limitándose a las páginas web y cuentas que <b>GRUPO ROKYS</b> mantiene en redes sociales), y a realizar las actividades necesarias para el cumplimiento de las finalidades antes descritas para las que ha consentido el tratamiento de sus datos personales. Asimismo, también podemos facilitar sus datos personales a organismos de la Administración Pública y autoridades competentes si somos requeridos de hacerlo en virtud de obligaciones establecidas en ley. En algunos casos, debemos también proporcionar su información personal a las referidas entidades con objeto del desarrollo de un proceso legal, judicial y/o administrativo, conforme a ley. 4. Ejercicio de sus derechos como titular de datos persona
</p>


<strong>6.	COLABORADORES</strong>
<p>
El presente apartado aplica únicamente si usted ha sido seleccionado como colaborador del <b>GRUPO ROKYS</b>o sus empresas relacionadas. Esta cláusula no aplica para clientes u otro tipo de usuarios. De haber sido seleccionado como colaborador del <b>GRUPO ROKYS</b> o sus empresas relacionadas, usted autoriza a que su información sea almacenada por dichas empresas, según corresponda. Asimismo, usted autoriza que se realice el tratamiento de todos los datos personales que suministre o se generen como consecuencia de su relación de prestación de servicios a favor <b>GRUPO ROKYS</b>o sus empresas relacionadas. Sus datos personales serán almacenados de manera indefinida o hasta que revoque dicha autorización. Usted autoriza que sus datos sean tratados, compartidos y transferidos <b>GRUPO ROKYS</b> y sus empresas relacionadas para las finalidades siguientes: i) gestión de recursos humanos; ii) administración de beneficios laborales y sociales para los colaboradores y sus derechohabientes o beneficiarios; iii) evaluación de desempeño; iv) registros de ingresos y salidas; v) gestión de programas corporativos, vi) manejo de acciones correctivas; vii) procesamiento y gestión de atenciones y reclamos de seguros, entidades prestadoras de servicios de salud y sistemas de pensiones; viii) evaluaciones de ingreso, salida y controles periódicos de salud; ix) gestión de capacitaciones y actividades recreacionales; x) administración de postulaciones a otros puestos; xi) difusión publicitaria internas y externas; xii) consultar y reportar a terceros información referida a antecedentes personales, profesionales, de desempeño, comportamiento crediticio y financiero del colaborador; xiii) trasladar su información a clientes o a empresas o entidades que requieran servicios en los que se solicite contar con la información personal y profesional de los colaboradores; xiv) compartir su información con socios comerciales, proveedores o empresas con las que <b>GRUPO ROKYS</b>o alguna de sus empresas relacionadas tenga un vínculo comercial, y que requieran conocer sus datos en atención a la labor que desempeñan o para supervisar el correcto funcionamiento y manejo de herramientas y programas que sean provistos por dichas empresas, como por ejemplo, terminales electrónicos de pago (POS), sistemas de pago, sistemas informáticos, sistemas de telecomunicaciones, entre otros; y, xv) ofrecimiento y prestación de servicios brindados por <b>GRUPO ROKYS</b>y sus empresas relacionadas o por terceros con los que este ha suscrito programas en beneficios de sus colaboradores, en cuyo caso se podrá trasladar esta información a dichos terceros, asegurando la seguridad y confidencialidad de sus datos personales. Por tal razón, la autorización para el tratamiento de sus datos personales resulta obligatoria para la ejecución de dichas actividades, y en caso de negativa, ellas no se podrán realizar. El tratamiento podrá ser realizado directamente por <b>GRUPO ROKYS</b>o alguna empresa relacionada o a través de un tercer agente identificado por <b>GRUPO ROKYS</b>o alguna empresa relacionada conforme a los términos y finalidades del presente consentimiento. En cualquier caso, <b>GRUPO ROKYS</b>y sus empresas relacionadas garantizan la seguridad y confidencialidad del tratamiento de sus datos personales. En cualquier caso, usted tiene la facultad de revocar o ejercer cualquiera de los derechos consagrados en la Ley N° 29733.
</p>

<strong>7.	USO DE COOKIES Y REDES SOCIALES</strong>
<p>
  El Titular de los Datos Personales que tenga acceso a la “Página Web”, acuerda recibir las Cookies que les transmitan los servidores de Servicios de <b>GRUPO ROKYS</b>Una cookie es todo tipo de “Archivo” o “Dispositivo” que se descarga en un “Equipo Terminal” (Ordenador, Teléfono Móvil, Tableta u otro) de cualquier “Titular de los Datos Personales” (tanto Persona Física como Persona Jurídica), con la finalidad de Almacenar Datos (desde pocos Kilobytes a muchos Megabytes) y que podrán ser actualizados y/o recuperados por la Entidad responsable de la instalación. Una Cookie no puede leer los datos o información del disco duro del Titular de los Datos Personales ni leer las Cookies creadas por otros sitios o páginas. Los presentes Términos y condiciones de Uso y Privacidad corresponden únicamente a los servicios de Servicios del <b>GRUPO ROKYS</b>La empresa no tiene control alguno sobre los sitios web mostrados como resultados de búsquedas o enlaces que se incluyen en nuestra “Página Web”. Es posible que estos sitios independientes envíen sus propias cookies u otros archivos a su equipo, recopilen datos o le soliciten que envíe información personal.
  Las redes sociales constituyen una plataforma de comunicación y de interconexión entre plataformas digitales de los distintos usuarios, las cuales son ajenas a Servicios de Franquicia GRUPO ROKYS. y, por ello, no se encuentran bajo su responsabilidad. La información y datos proporcionados por el Titular de los Datos Personales dentro de las redes sociales de las cuales <b>GRUPO ROKYS</b>sea usuario, no constituyen en ningún momento parte de la información personal o sensible sujeta a la protección de este Política de Privacidad, siendo responsabilidad de la empresa prestadora de esa plataforma y de quien lo publica.

</p>

<strong>8.	MODIFICACIONES A LA PRESENTE POLÍTICA DE PRIVACIDAD</strong>
<p>
Con motivo de la mejora continua de nuestros procesos, <b>GRUPO ROKYS</b>podrán realizar modificaciones y correcciones a esta Política de Privacidad, a las cuales usted da conformidad. Por favor, verifique estos términos regularme para consultar los cambios que puedan haber existido.
</p>




        </div>
      </md-dialog-content>
    </form>
  </md-dialog>
</script>

<script type="text/ng-template" id="templatePoliticas.html">
  <md-dialog aria-label="Mango (Fruit)" class="dialog-preguntas">
    <form ng-cloak>

      <md-toolbar>
        <div class="md-toolbar-tools">
          <md-button class="md-icon-button" ng-click="cancel()" style="right: 20px;position: absolute;">
            <i class="fas fa-times"></i>
          </md-button>
        </div>
      </md-toolbar>

      <md-dialog-content class="md-dialog-content--top50">

        <div class="md-dialog-content dialog-preguntas__content">
          <h2>Términos y condiciones</h2>

          <p>- Sorteo válido por consumo de la promoción de 2 pies de manzana a S/.5.00 en los locales de <br>Roky´s de Lima y Provincias. </p>
          <p>- Válido para mayores de 18 años que residan en Perú.</p>
          <p>- El premio es un vale de 1 pollo a la brasa con papas fritas + un pack futbolero (1 maletín + 1 minipelota + 1 pelota).</p>
          <p>- El premio no es reemplazable por dinero en efectivo ni por otro producto.</p>
          <p>- Fecha de sorteo 17 de diciembre del 2019. Serán 60 los afortunados ganadores.</p>
          <p>- Solo aplica para boletas y facturas desde el 12 de noviembre del 2019.</p>
          <p>- El recojo del premio se dará en nuestras oficinas administrativas previa coordinación mediante nuestras redes sociales, el recojo es presencial.</p>
          <p>- Si el ganador no recoge su premio hasta el 21 de diciembre del 2019, lo pierde automáticamente sin lugar a reclamo. </p>
          <p>- Términos y condiciones sujetas a cambios sin previo aviso. </p>
        </div>
      </md-dialog-content>

    </form>
  </md-dialog>
</script>
