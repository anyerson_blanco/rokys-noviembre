<div class="box_carousel wow animated fadeIn" data-wow-delay="0.2s">
  <div class="owl-carousel owl-theme grid">

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador1.jpg') }}">
        <figcaption>
          <p>Nombres: Lourdes Elvira Paredes Suárez
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">46636789</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">18/09/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador2.jpg') }}">
        <figcaption>
          <p>Nombres: Jorge Ricardo Roca Cuadros
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">47887969</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">18/09/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador3.jpg') }}">
        <figcaption>
          <p>Nombres: Javier Ylaquita Berroa
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">29407575</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">25/09/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador4.jpg') }}">
        <figcaption>
          <p>Nombres: Cynthia Melgar Pacheco
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">44318727</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">25/09/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador5.jpg') }}">
        <figcaption>
          <p>Nombres: Sonia Janneth Vela Linares
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">06066292</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">02/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador6.jpg') }}">
        <figcaption>
          <p>Nombres: Carla Maria Del Carpio Herrera
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">46625849</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">02/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador7.jpg') }}">
        <figcaption>
          <p>Nombres: Vicente Santos Andres Ttito
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">09741958</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">09/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador8.jpg') }}">
        <figcaption>
          <p>Nombres: Leidy Cinthia Palacios Vertiz
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">43412279</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">09/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador9.jpg') }}">
        <figcaption>
          <p>Nombres: Cesar Augusto Vizcardo Ramirez
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">07747895</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">16/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador10.jpg') }}">
        <figcaption>
          <p>Nombres: Rosa De Los Angeles Rivas Garcia
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">46111432</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">16/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador11.jpg') }}">
        <figcaption>
          <p>Nombres: Carmen Rosa Maury Rodriguez
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">45810840</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">23/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador12.jpg') }}">
        <figcaption>
          <p>Nombres: Jhoner Espeza Molina
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">40583050</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">23/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador13.jpg') }}">
        <figcaption>
          <p>Nombres: Leonela Giuliana Mendoza Valeriano
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">43040953</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">30/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador14.jpg') }}">
        <figcaption>
          <p>Nombres: Himblert Arias Araoz
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">10658595</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">30/10/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador15.jpg') }}">
        <figcaption>
          <p>Nombres: Janet Alessandra Garcia Guardado
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">47428488</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">06/11/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>

    <div class="item">
      <figure class="effect-oscar">
        <img src="{{ url('/img/general/ganadores/ganador16.jpg') }}">
        <figcaption>
          <p>Nombres: Anthony Mickol Ramírez Oyola
            <br> Documento: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">75111189</span>
            <br> Fecha de sorteo: <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">06/11/2017</span></p>
          <a href="javascript:;">Ver más</a>
        </figcaption>
      </figure>
    </div>
    <!--
    <div class="item">
      <figure >
        <img src="{{ url('/img/general/ganadores/muy_pronto.jpg') }}">

      </figure>
    </div>-->

  </div>
</div>
