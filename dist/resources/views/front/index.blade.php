<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>

  @include('partials/header')
  <link rel="stylesheet" href="{{ url('/css/style.css?V=2.8') }}">
  <link rel="icon" type="image/x-icon" href="{{ url('/img/favicon.ico') }}" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <!-- SCRIPTS
===============================================-->
  <!-- IE FILES
===============================================-->
  <!--[if lt IE 9]>
  <script src="ie/html5shiv.js"></script>
  <script src="ie/respond.min.js"></script>
  <![endif]-->
  <!-- Global site tag (gtag.js) - Google Analytics -->
</head>
<body ng-app="tvsApp" id="section-0" class="is-loading">
  <div ng-controller="mainController as ctrl">
    <div class="wrapper-principal">
      <!--Contenidos-->
        <div id="fullpage">
          <div class="section fullpage__home toload" id="Ainicio">
            <div class="step1 animate slide-right pagina1" ng-show="paso1">
              <div class="pagina1__seccion1">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-12 col-12">
                      <div class="pagina2__contenedor">
                        <img src="{{ url('/img/frase.png') }}" alt="" class="pagina2__contenedor__frase wow bounceInRight img-fluid" data-wow-delay="0.80s" data-wow-duration="1s">
                        <img src="{{ url('/img/papas.png') }}" alt="" class="pagina2__contenedor__papas wow bounceInRight img-fluid" data-wow-delay="0.80s" data-wow-duration="1.5s">
                        <img src="{{ url('/img/minipelota.png') }}" alt="" class="pagina2__contenedor__minipelota wow bounceInRight img-fluid" data-wow-delay="0.80s" data-wow-duration="2s">
                        <img src="{{ url('/img/pollo.png') }}" alt="" class="pagina2__contenedor__pollo wow bounceInRight img-fluid" data-wow-delay="0.80s" data-wow-duration="2.5s">
                        <img src="{{ url('/img/pelota.png') }}" alt="" class="pagina2__contenedor__pelota wow bounceInRight img-fluid" data-wow-delay="0.80s" data-wow-duration="3s">
                        <img src="{{ url('/img/maletin.png') }}" alt="" class="pagina2__contenedor__bolso wow bounceInRight img-fluid" data-wow-delay="0.80s" data-wow-duration="3.5s">
                        <div class="pagina1__contenedor__boton">
                          <a href="#" ng-click="siguiente();$event.preventDefault()" >descrubre cómo</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="step2 animate seccion2 slide-right" ng-show="paso2">
              <div class="d-flex align-items-center pagina1__seccion1">
                <div class="container">
                <div class="row wow bounceInRight">
                  <div class="col-lg-12">
                    <div class="seccion2__intro">
                      <h2>¡Participa!</h2>
                      <h3>Por la compra de 2 pie de manzana, accede al sorteo de uno de los 60 paquetes del hincha.</h3>
                    </div>
                  </div>
                  <div class="col-lg-8 col-xl-6 m-auto">
                    <div class="seccion2__formulario">
                      <div class="seccion2__formulario__intro">
                        <h3>Ingresa tus datos en el siguiente formulario:</h3>
                      </div>
                      <div class="seccion2__formulario__form">
                        <form class="" name="formCodigo">
                          <div class="row">
                            <div class="col-lg-6">
                              <input type="text" name="nombre" id="nombre" name="" value="" required minlength="5" placeholder="Nombre..." class="form-control" ng-model="user.nombres" required>
                              <label for="nombre" >Nombres y apellidos</label>
                            </div>
                            <div class="col-lg-6">
                              <input type="number" id="numero" name="" value="" class="form-control" required maxlength="9" placeholder="969-756-876" ng-model="user.telefono" required>
                              <label for="">Télefono (celular)</label>
                            </div>
                            <div class="col-lg-6">
                              <input type="number" name="" value="" id="dni" placeholder="Ingrese DNI" required maxlength="8" class="form-control" ng-model="user.documento" required>
                              <label for="">DNI (8 Caracteres)</label>
                            </div>
                            <div class="col-lg-6">
                              <input type="email" name="" value="" id="correo" placeholder="Ingrese correo" class="form-control" ng-model="user.email" required>
                              <label for="">Correo</label>
                            </div>
                            <div class="col-lg-12">
                              <input type="text" id="boleta" name="" value="" placeholder="Número de boleta" class="form-control" ng-model="user.boleta" required>
                              <label for="">Número de factura o boleta</label>
                            </div>
                            <div class="col-lg-12">
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="checkboxTerminos2" name="terminos" required="required" ng-model="user.sdd">
                                <label class="custom-control-label" for="checkboxTerminos2">
                                  He leído y acepto los <span class="terminos_link" ng-click="showModal($event,'templatePoliticas.html')">términos y condiciones</span> de la campaña
                                </label>
                              </div>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="checkboxTerminos" name="terminos2" required="required" ng-model="user.terminos">
                                <label class="custom-control-label" for="checkboxTerminos">
                                  He leído la <span class="terminos_link" ng-click="showModal($event,'templateTerminos.html')">autorización y acepto el tratamiento de mis datos personales</span> de Rokys.
                                </label>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                              <div class="seccion2__formulario__form__enviar">
                                <a href="" ng-click="validarDatos(user, formCodigo.$valid);$event.preventDefault()">Enviar</a>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                              <div class="seccion2__formulario__form__atras">
                                <a href="" ng-click="anterior();$event.preventDefault()"><i class="fas fa-home"></i> VOLVER al home</a>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="seccion2__formulario__final">
                        <span class="sorteo">Fecha del sorteo:</span> <span>17 de diciembre de 2019</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
            <div class="fullpage__home__imgAbajo">

            </div>
            <section class="footer">
            <div class="container">
              <div class="row">
                <div class="col-lg-5 ">
                  <div class="footer__terminos">
                    <ul>
                      <li ng-click="showModal($event,'templateTerminos.html')" style="cursor:pointer;">Declaración de protección de datos</li>
                      <li ng-click="showModal($event,'templatePoliticas.html')" style="cursor:pointer;">Términos y condiciones</li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-4">
                </div>
                <div class="col-lg-3">
                  <div class="footer__copy">
                    <p>2019 Todos los derechos Reservados - <a href="https://webtilia.com/" class="webtilia" target="_blank">Webtilia</a></p>
                  </div>
                </div>
              </div>
            </div>
            </section>
          </div>
        </div>

      <!--Fin footer-->
      @include('partials/scripts') @include('partials/modals')
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
      <script>
        AOS.init();
      </script>
      <script type="text/javascript">
      $(document).on("click", ' .btn-link', function() {
                longitud = $(this).attr("iframe");
      });
        var URL_SITE = '{{url()}}';
        var TOKEN = '{{ csrf_token() }}';
        $(".form-check-input").click(function(){
          if($(this)[0].checked){
            $(this).siblings('span').addClass('activo');
          }else{
            $(this).siblings('span').removeClass('activo');
          }
        });
        $('.down a').click(function () {
          $('body,html').animate({
            scrollTop: $('#formulario').height() + $('#formulario').offset().top
          }, 800);
          return false;
        });
      </script>
      <script src="{{ url('/js/app.js?V=2.1') }}"></script>
      <script src="{{ url('/js/controllers/main.js?V=2.2') }}"></script>
      <script src="{{ url('/js/services/codes.js?V=2.2') }}"></script>
    </div>
  </div>
</body>
</html>
