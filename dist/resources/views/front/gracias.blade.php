<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>

  @include('partials/header')
  <link rel="stylesheet" href="{{ url('/css/style.css?V=2.8') }}">
  <link rel="icon" type="image/x-icon" href="{{ url('/img/favicon.ico') }}" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,700i,800&display=swap" rel="stylesheet">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <!-- SCRIPTS
===============================================-->
  <!-- IE FILES
===============================================-->
  <!--[if lt IE 9]>
  <script src="ie/html5shiv.js"></script>
  <script src="ie/respond.min.js"></script>
  <![endif]-->
  <!-- Global site tag (gtag.js) - Google Analytics -->
</head>
<body ng-app="tvsApp" id="section-0" class="is-loading">
  <div ng-controller="mainController as ctrl">
    <div class="wrapper-principal">
      <!--Contenidos-->
        <div id="fullpage">
          <div class="section fullpage__home toload" id="Ainicio">

            <div class="step2 animate seccion3 slide-right" ng-show="paso1">

              <div class="container">
                <div class="row">
                  <div class="col-lg-4 m-auto">
                    <div class="gracias d-flex align-items-center justify-content-center">
                      <div class="gracias__contenido">
                          <img src="{{ url('/img/logo.png') }}" alt="" class="img-fluid logo">
                          <div class="gracias__contenido--1">
                            <img src="{{ url('/img/participando.png') }}" alt="" class="img-fluid">
                            <h2>Mucha suerte</h2>
                            <div class="gracias__contenido--1__volver">
                              <a href="#">VOLVER</a>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <section class="footer">
            <div class="container">
              <div class="row">
                <div class="col-lg-5">
                  <div class="footer__terminos">
                    <ul>
                      <li>Declaración de protección de datos</li>
                      <li>Términos y condiciones</li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-4">
                </div>
                <div class="col-lg-3">
                  <div class="footer__copy">
                    <p>2019 Todos los derechos Reservados - Webtilia</p>
                  </div>
                </div>
              </div>
            </div>
            </section>
          </div>
        </div>

      <!--Fin footer-->
      @include('partials/scripts') @include('partials/modals')
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
      <script>
        AOS.init();
      </script>
      <script type="text/javascript">
      $(document).on("click", ' .btn-link', function() {
                longitud = $(this).attr("iframe");
      });
        var URL_SITE = '{{url()}}';
        var TOKEN = '{{ csrf_token() }}';
        $(".form-check-input").click(function(){
          if($(this)[0].checked){
            $(this).siblings('span').addClass('activo');
          }else{
            $(this).siblings('span').removeClass('activo');
          }
        });
        $('.down a').click(function () {
          $('body,html').animate({
            scrollTop: $('#formulario').height() + $('#formulario').offset().top
          }, 800);
          return false;
        });
      </script>
      <script src="{{ url('/js/app.js?V=2.1') }}"></script>
      <script src="{{ url('/js/controllers/main.js?V=2.2') }}"></script>
      <script src="{{ url('/js/services/codes.js?V=2.2') }}"></script>
    </div>
  </div>
</body>
</html>
