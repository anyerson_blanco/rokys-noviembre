<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
<link rel="stylesheet" href="{{ url('/css/materialize.min.css?V=1.5') }}">
<style>

body{
  background:#e01f27;
  color: white;
}
h5{
  text-align:center;margin-top:50px;padding-bottom:20px
}
</style>
</head>

<body>

  <h5><strong>Proyección diaria</strong></h5>

      <div class="app">
           <center>
               {!! $chart->html() !!}
           </center>
       </div>

        {!! Charts::scripts() !!}
        {!! $chart->script() !!}

<div style="float:right;padding-right:20px;">TOTAL: {{ $total }}</div>
<h5><strong>Usuarios participantes</strong></h5>
          <table>
                  <thead>
                    <tr>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Documento</th>
                        <th>Teléfono</th>
                        <th>Email</th>
                        <th>Mercado</th>
                        <th>Fecha de participación</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach($users as $key=>$user)
                    <tr>
                      <td>{{ $user->nombres }}</td>
                      <td>{{ $user->apellidos }}</td>
                      <td>{{ $user->num_documento }}</td>
                      <td>{{ $user->telefono }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->mercado }}</td>
                      <td>{{ $user->created_at }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

        <p style="text-align:center"><a href="{{ url('/descargar') }}"><button class="btn waves-effect waves-light orange" type="button" name="button">Descargar Registrados</button></a></p>

</body>
</html>
