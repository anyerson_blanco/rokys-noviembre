¡Hola! Lamentamos que no hayas podido recordar la contraseña que ingresaste al momento de registrarte.<br/>
Pero no te preocupes, puedes generar una nueva contraseña haciendo clic en el siguiente enlace: <a href="{{ url('password/reset/'.$token) }}">{{ url('password/reset/'.$token) }}</a><br/><br/>
Atentamente, el equipo <strong>Tampico Airlines</strong>
