	// create the module and name it tampicoApp

	var tvsApp = angular.module('tvsApp', ['ngSanitize', 'ngMaterial', 'ngAnimate' , 'starter.services', 'blockUI','duScroll', 'cp.ngConfirm'],
	function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
  })
  .config(function($mdDateLocaleProvider, $httpProvider, $mdThemingProvider, $mdIconProvider) {
	  $mdDateLocaleProvider.formatDate = function(date) {
	    return date ? moment(date).format('DD-MM-YYYY') : '';
	  };
		$mdThemingProvider.theme('default')
    .primaryPalette('indigo')
    .accentPalette('orange');
	})
	.constant('jQuery', window.jQuery)
	;
