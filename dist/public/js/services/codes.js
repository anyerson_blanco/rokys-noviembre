angular.module('starter.services', [])

  .service('CodeService', function($http, $q) {

      var ubigeo = function() {
        var _apiAddress = URL_SITE + '/ubigeo';
        var defered = $q.defer();
        var promise = defered.promise;


        $http({
          method: 'GET',
          url: _apiAddress,
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Data-Type': 'json'
          }
        }).then(
          function successCallback(response) {
            defered.resolve(response.data);
          },
          function errorCallback(response) {
            defered.reject(response.status);
          }
        );
        return promise;
      };

      var retornarProvincias = function($params) {
        var _apiAddress = URL_SITE + '/provincias';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
          method: 'POST',
          url: _apiAddress,
          data: 'departamento=' + $params.departamento,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
          }
        }).then(
          function successCallback(response) {
            defered.resolve(response.data);
          },
          function errorCallback(response) {
            defered.reject(response.status);
          }
        );
        return promise;
      };

      var retornarDistritos = function($param1, $param2) {
        var _apiAddress = URL_SITE + '/distritos';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
          method: 'POST',
          url: _apiAddress,
          data: 'departamento=' + $param1.departamento +'&provincia=' + $param2.provincia,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
          }
        }).then(
          function successCallback(response) {
            defered.resolve(response.data);
          },
          function errorCallback(response) {
            defered.reject(response.status);
          }
        );
        return promise;
      };

      var sendToken = function($params) {
        var _apiAddress = URL_SITE + '/password/email';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
          method: 'POST',
          url: _apiAddress,
          data: 'email=' + $params.email,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            }
        }).then(
          function successCallback(response) {
            defered.resolve(response.data);
          },
          function errorCallback(response) {
            defered.reject(response.status);
          }
        );
        return promise;
      };


      var changePass = function($params) {
        var _apiAddress = URL_SITE + '/password/reset';
        var defered = $q.defer();
        var promise = defered.promise;

        console.log($params);

        $http({
          method: 'POST',
          url: _apiAddress,
          data: 'email=' + $params.email +'&password=' + $params.password + '&password_confirmation=' +$params.cpassword + '&token='+ $params.token,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
          }
        }).then(
          function successCallback(response) {
            defered.resolve(response.data);
          },
          function errorCallback(response) {
            defered.reject(response.status);
          }
        );
        return promise;
      };

      var validateTraveler = function($params) {
        var _apiAddress = URL_SITE + '/api/vusrp';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
          method: 'POST',
          url: _apiAddress,
          data: JSON.stringify($params),
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Data-Type': 'json'
          }
        }).then(
          function successCallback(response) {
            defered.resolve(response.data);
          },
          function errorCallback(response) {
            defered.reject(response.status);
          }
        );
        return promise;
      };

      var validateUser = function($params) {
        var _apiAddress = URL_SITE + '/api/vusr';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
          method: 'POST',
          url: _apiAddress,
          data: JSON.stringify($params),
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Data-Type': 'json'
          }
        }).then(
          function successCallback(response) {
            defered.resolve(response.data);
          },
          function errorCallback(response) {
            defered.reject(response.status);
          }
        );
        return promise;
      };

      var validateMotor = function($params) {

        console.log(JSON.stringify($params))
        var _apiAddress = URL_SITE + '/api/vmotor';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
            method: 'POST',
            url: _apiAddress,
            data: JSON.stringify($params),
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
              'Data-Type': 'json'
            } 
          }).then(
            function successCallback(response) {
              defered.resolve(response.data);
            },
            function errorCallback(response) {
              defered.reject(response.status);
            }
          );

        return promise;
      };

      var insertUser = function($params) {

        console.log(JSON.stringify($params))
        var _apiAddress = URL_SITE + '/api/rcode';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
            method: 'POST',
            url: _apiAddress,
            data: JSON.stringify($params),
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
              'Data-Type': 'json'
            }
          }).then(
            function successCallback(response) {
              defered.resolve(response.data);
            },
            function errorCallback(response) {
              defered.reject(response.status);
            }
          );
        return promise;
      };

      var updateSocio = function($params) {

        //console.log(JSON.stringify($params))
        var _apiAddress = URL_SITE + '/api/rusrp';
        var defered = $q.defer();
        var promise = defered.promise;

        //onsole.log(JSON.stringify($params));

        $http({
            method: 'POST',
            url: _apiAddress,
            data: JSON.stringify($params),
            headers:{"Authorization": "Bearer " + $params.token,
               "Content-Type": "application/json;charset=UTF-8"
            }
          }).then(
            function successCallback(response) {
              defered.resolve(response.data);
            },
            function errorCallback(response) {
              defered.reject(response.status);
            }
          );
        return promise;
      };

      var setRooms = function($params) {

        //console.log(JSON.stringify($params))
        var _apiAddress = URL_SITE + 'servicios/habitacionController/habitaciones_update';
        var defered = $q.defer();
        var promise = defered.promise;

        $http({
            method: 'POST',
            url: _apiAddress,
            data: JSON.stringify($params),
            headers:{"Authorization": "Bearer " + $params.token,
               "Content-Type": "application/json;charset=UTF-8"
            }
          }).then(
            function successCallback(response) {
              defered.resolve(response.data);
            },
            function errorCallback(response) {
              defered.reject(response.status);
            }
          );
        return promise;
      };

      return {
        ubigeo: ubigeo,
        retornarProvincias: retornarProvincias,
        retornarDistritos: retornarDistritos,
        sendToken: sendToken,
        validateUser: validateUser,
        changePass: changePass,
        validateTraveler: validateTraveler,
        updateSocio: updateSocio,
        insertUser: insertUser,
        setRooms: setRooms,
        validateMotor: validateMotor
      };
    });
