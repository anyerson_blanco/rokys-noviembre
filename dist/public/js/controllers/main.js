tvsApp.controller('mainController', function($scope, $rootScope, CodeService, $mdDialog, $location, $document, $ngConfirm, $timeout) {

  $scope.ganoKms=false;
  $scope.paso1 = true;
  $scope.paso2 = false;
  $scope.paso3 = false;
  $scope.mensaje_motor = false;
  $scope.mensaje_chasis = false;
  $scope.chasis_count = 0;
  $scope.motor_count = 0;
  $scope.passMatchErrMsg = true;
  $scope.usuarioExistente=false;
  $scope.dataModificablePorUsuario={};
  $scope.login=false;
  $scope.user = {};
  $scope.viajero = {};
  $scope.numkms=0;
  $scope.mensajePrompt="";
  $scope.theme = $scope.theme || 'default';
  $scope.textoTip="";

  $scope.depas = {};
  $scope.provs = {};

this.myDate = new Date();


  this.minDate = new Date(
    this.myDate.getFullYear(),
    7,
    1
  );

  this.maxDate = new Date(
    this.myDate.getFullYear(),
    6,
    30
  );

  $scope.validate_motor = function() {

    if($scope.user.motor && $scope.user.motor.length > 0 && $scope.user.motor.length != 12 ){
      $scope.mensaje_motor = true;
      $scope.motor_count = $scope.user.motor.length;
    }else{
      $scope.mensaje_motor = false;
      $scope.motor_count = 0;
    }

  }

  $scope.validate_chasis = function() {

    if($scope.user.chasis && $scope.user.chasis.length > 1 && $scope.user.chasis.length != 17 ){
      $scope.mensaje_chasis = true;
      $scope.chasis_count = $scope.user.chasis.length;
    }else{
      $scope.mensaje_chasis = false;
      $scope.chasis_count = 0;
    }

  }

  $scope.siguiente = function() {
      // showAlert("Lo sentimos, la campaña ha finalizado");

    $scope.paso1 = false;
    $scope.paso2 = true;
    $scope.paso3 = false;
    $scope.paso4 = false;
  }

  $scope.anterior = function() {
    $scope.paso1 = true;
    $scope.paso2 = false;
    $scope.paso3 = false;
    $scope.paso4 = false;
  }

  $scope.datosPersonales = function() {

    $scope.paso1 = false;
    $scope.paso2 = false;
    $scope.paso3 = true;
    $scope.paso4 = false;
  }

  $scope.gracias = function() {

    $scope.paso1 = false;
    $scope.paso2 = false;
    $scope.paso3 = false;
    $scope.paso4 = true;
  }


  $scope.user = {
      combustible : 'GASOLINA',
      fecha : new Date(),
      departamento : "LIMA",
  };


  // CodeService.ubigeo().then(function(data) {
  //  $scope.depas = data;
  //
  //  $timeout(function(){
  //     $('select#departamentoSelect').niceSelect('update');
  //  }, 200);
  //
  // });

  $scope.updateProvincia = function() {
   var departamento = $scope.user.departamento;

   CodeService.retornarProvincias(departamento).then(function(data) {
    $scope.provs = data;

    $timeout(function(){
       $('select#provinciaSelect').niceSelect('update');
    }, 200);

   });

 }

 $scope.updateDistrito = function() {
  var departamento = $scope.user.departamento;
  var provincia = $scope.user.provincia;

  console.log(provincia);

  CodeService.retornarDistritos(departamento, provincia).then(function(data) {
   $scope.dists = data;

   $timeout(function(){
      $('select#distritoSelect').niceSelect('update');
   }, 200);

  });

}


  $scope.validarCodigo = function(objectToSend, formValid) {

    // showAlert("La campaña ha llegado a su fin.");

    if (formValid) {

      CodeService.validateMotor(objectToSend).then(function(data) {
        if(data.rpta=="3"){
          showAlert("El número de boleta ingresado ya a sido ingresado anteriormente");
          return;
        }
        if (data.rpta == "1") {
          window.locationf="/gracias";
          // $scope.paso1 = false;
          // $scope.paso2 = false;
          // $scope.paso3 = true;
          // $scope.paso4 = false;
        }
      });
    } else {
      showAlert("Ingresa los datos y acepta los términos y condiciones")
    }



  };

  $scope.validarDocumento = function(objectToSend, formValid) {

    if (formValid) {

      if ($scope.paso2_2) {
          $scope.paso2 = false;
          $scope.paso3 = true;
          return;
      }

      CodeService.validateUser(objectToSend).then(function(data) {
        if (data.estado == "1") {
          $scope.user.nombre1=data.user.nombre1;
          $scope.user.nombre2=data.user.nombre2;
          $scope.user.apellido1=data.user.apellido1;
          $scope.user.apellido2=data.user.apellido2;
          $scope.user.tipodocumento=data.user.tipo_documento;
          $scope.user.numdocumento=data.user.num_documento;
          $scope.user.email=data.user.email;
          $scope.user.telefono=data.user.telefono;

          $scope.dataModificablePorUsuario={
            nombre1:data.user.nombre1,
            nombre2:data.user.nombre2,
            apellido1:data.user.apellido1,
            apellido2:data.user.apellido2,
            email:data.user.email,
            telefono:data.user.telefono
          }

          $scope.paso2 = false;
          $scope.paso3 = false;
          $scope.paso4 = true;

          $scope.usuarioExistente=true;
          /*$scope.paso2_2 = true;
          $scope.usuarioExistente=true;
          $scope.mensajeContrasenia="INGRESA TU CONTRASEÑA";*/
        } else {
          //usuario nuevo
          $scope.user.nombre1="";
          $scope.user.nombre2="";
          $scope.user.apellido1="";
          $scope.user.apellido2="";
          $scope.user.email="";
          $scope.user.telefono="";
          $scope.user.socio="";


          $scope.usuarioExistente=false;
          $scope.mensajeContrasenia="CREA UNA CONTRASEÑA Y MIRA TUS KMS CUANDO GUSTES.";
          $scope.paso2_2 = true;
        }

      });
    } else {
      if ($scope.paso2_2) {
        showAlert("El documento de identidad o el password ingresado no son correctos")
      } else {
        showAlert("El documento de identidad no es correcto")
      }
    }

  };

  $scope.validarDatos = function(objectToSend, formValid) {

    if (formValid) {

      grabarDatos($scope.user);

    } else {
      if($("#nombre").val().length < 1) {
        showAlert("Ingresa tus nombres y apellidos")
      }else{
        if($("#numero").val().length < 1) {
          showAlert("Ingresa tu número correctamente")
        }else{
          if($("#dni").val().length < 1) {
            showAlert("Ingresa tu tu DNI de 8 digitos")
          }else{
            if($("#correo").val().length < 1) {
              showAlert("Ingresa tu correo")
            }else{
              if($("#boleta").val().length < 1) {
                showAlert("Ingresa la boleta correctamente")
              }
            }
          }
        }
      }

    }

  };

  $scope.cpassValidate = function() {
    //equal to password or not?----------------------------------------
    if (($scope.user.password != $scope.user.cpassword) && !$scope.usuarioExistente) {
      $scope.passMatchErrMsg = true;
    } else {
      $scope.passMatchErrMsg = false;
    }
  }

  $scope.retornarPaso2=function(){
    $scope.paso2 = true;
    $scope.paso3 = false;
  }

  $scope.retornarPaso3=function(){
    if($scope.usuarioExistente){
      $scope.paso2 = true;
      $scope.paso4 = false;
    }else{
      $scope.paso3 = true;
      $scope.paso4 = false;
    }

  }

  function showAlert(mensaje) {

    $ngConfirm({
      title: 'Te informamos:',
      content: mensaje,
      columnClass: 'small',
      type: 'red',
      icon: 'fa fa-info-circle',
      theme: 'modern',
      closeIcon: true,
      typeAnimated: true,

    });
  }

  function grabarDatos(objectToSend) {

    CodeService.insertUser(objectToSend).then(function(data) {
      if(data.rpta=="3"){
        showAlert("El número de boleta a sido registrado anteriormente.");
        return;
      }
      if (data.rpta == "1") {
        window.location.href = BASE_URL + "/gracias";
        //fbq('track', 'CompleteRegistration');
      }
    });
  };

  $scope.showDiv=function(texto){
    $scope.textoTip=texto;
  }

  $scope.hideDiv=function(texto){
    $scope.textoTip="";
  }

  $scope.validarViajero=function(objectToSend, formValid){
    if (formValid) {

      CodeService.validateTraveler(objectToSend).then(function(data) {
        if (data.estado == "1") {
          $scope.login=true;
          $scope.viajero=data.user;
          $scope.numkms=data.numkms;
          var porc=0;
          if($scope.numkms>=11000){
            porc=100;
          }else{
            porc=(100*($scope.numkms-850))/11000;
          }

          if($scope.numkms==1000){
            porc=5;
          }
          if($scope.numkms==500){
            porc=3;
          }
          var bar = angular.element(document.getElementById('bar'));
          bar.barIndicator('loadNewData', [porc]);
          $scope.viajero.token = data.token;

        } else {
          $scope.login=false;
          showAlert("La contraseña o el documento de identidad ingresado no son correctos");
        }
      });


    } else {
      showAlert("Ingresa tu número de documento y contraseña correctamente");
    }
  }

  $scope.updateNumSocio=function(objectToSend, formValid){
    if (formValid) {

      CodeService.updateSocio(objectToSend).then(function(data) {
        if (data.estado == "1") {
          showAlert("Tu número de socio LATAM ha sido actualizado correctamente");
        } else {
          showAlert("Lo sentimos, ha ocurrido un error, inténtalo nuevamente");
        }
      });


    } else {
      showAlert("El número de socio no es correcto.");
    }
  }




  $scope.gotoKms = function() {
    var section3 = angular.element(document.getElementById('section-3'));
    $document.scrollToElementAnimated(section3);
  }


  $scope.showModal = function(ev, template) {
    //console.log($scope.token_csrf);
    $mdDialog.show({
        controller: ['$scope', '$mdDialog', function($scope, $mdDialog) {
          // controller logic
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
        }],
        templateUrl: template,
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: $scope.customFullscreen
      })
      .then(function(answer) {
        $scope.status = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });

  };


  $scope.resetRegistro=function(){
    $scope.paso1 = true;
    $scope.paso2 = false;
    $scope.paso3 = false;
    $scope.paso4 = false;
    $scope.passMatchErrMsg = true;
    $scope.usuarioExistente=false;
    $scope.dataModificablePorUsuario={};
    $scope.user = {};

    $(".form-check-input").siblings('span').removeClass('activo');

    $scope.user = {
        combustible : 'GASOLINA',
        fecha : new Date(),
        departamento : "LIMA",
        terminos: false
    };

    $timeout(function(){
    	 $('select#departamentoSelect').niceSelect('update');
       $('select#provinciaSelect').niceSelect('update');
       $('select#distritoSelect').niceSelect('update');
	  }, 200);

  }

  $scope.openPromptSocio=function(){
    $ngConfirm({
      title: '',
      content: 'Inscríbete en LATAM PASS. Únete, de manera fácil y rápida. Como socio podrás acumular kilómetros, canjear pasajes, canjear productos en el catálogo y acceder a exclusivos beneficios de LATAM PASS. <br /><b>Recuerda que debes ser socio LATAM Pass para poder canjear tus kms. acumulados en nuestra web.</b> Si aún no eres socio LATAM Pass podrás registrar tu número más adelante en la sección "TUS KMS ACUMULADOS"',
      columnClass: 'medium',
      type: 'orange',
      theme: 'modern',
      closeIcon: true,
      typeAnimated: true
    });
  }

  $scope.showPrompt = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    /*var confirm = $mdDialog.prompt()
      .title('¿Quieres ver tus kms. pero te olvidaste tu contraseña?')
      .textContent('Para poder ayudarte necesitamos que ingreses el correo que registraste.')
      .placeholder('Tu correo')
      .ariaLabel('Tu correo')
      .initialValue('')
      .targetEvent(ev)
      .ok('Aceptar')
      .cancel('Cancelar');

    $mdDialog.show(confirm).then(function(result) {
      if(!validateEmail(result)){
          $scope.mensajePrompt = 'El formato de correo ingresado no es correcto';
      }else{
        CodeService.sendToken({"email":result,"token":getXsrfToken()}).then(function(data) {
          if (data.estado == "1") {
            $scope.mensajePrompt = '¡Listo! Hemos enviado unas instrucciones a '+ result +' para que pronto puedas ingresar a esta sección.';
          }
          if (data.estado == "0") {
            $scope.mensajePrompt = 'El correo ingresado no se encuentra registrado en nuestra base de datos';
          }
        });
      }
    //  $scope.mensajePrompt = 'You decided to name your dog ' + result + '.';
    }, function() {
      $scope.mensajePrompt = '';
    });*/

            $ngConfirm({
                            title: '¿Olvidaste tu contraseña?',
                            icon: 'fa fa-exclamation-circle',
                            columnClass: 'medium',
                            content:'<div><div class="form-group"><label class="control-label">Ingresa tu correo electrónico registrado en Tampico Airlines.</label><input autofocus type="text" ng-model="recuperar" id="input-name" placeholder="Ingresa tu correo" class="form-control"></div></div>',
                            type: 'orange',
                            icon: 'fa fa-exclamation-triangle',
                            closeIcon: true,
                            theme: 'light',
                            typeAnimated: true,
                            buttons: {
                                Cerrar: function (scope) {
                                    //console.log(scope.recuperar);
                                },
                                tryAgain: {
                                    text: 'Enviar a mi correo',
                                    btnClass: 'btn-warning',
                                    action: function(scope){

                                      if(!validateEmail(scope.recuperar)){
                                          $ngConfirm('El formato de correo ingresado no es correcto');
                                        }else{
                                        CodeService.sendToken({"email":scope.recuperar,"token":getXsrfToken()}).then(function(data) {
                                          if (data.estado == "1") {
                                            $ngConfirm('¡Listo! Hemos enviado unas instrucciones a '+ scope.recuperar +' para que pronto puedas ingresar a esta sección.');
                                          }
                                          if (data.estado == "0") {
                                            $ngConfirm('El correo ingresado no se encuentra registrado en nuestra base de datos');
                                          }
                                        });
                                      }
                                    }
                                }
                            }
                        })
          };

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  var getXsrfToken = function() {
    var cookies = document.cookie.split(';');
    var token = '';

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].split('=');
        if(cookie[0] == 'XSRF-TOKEN') {
            token = decodeURIComponent(cookie[1]);
        }
    }

    return token;
}


});
