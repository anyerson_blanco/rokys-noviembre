tampicoApp.controller('passwordController', function($scope, $rootScope, CodeService, $mdDialog, $location, $document, $ngConfirm, $window) {


  $scope.passMatchErrMsg = false;
  $scope.user = {};
  $scope.theme = $scope.theme || 'default';


  function showAlert(mensaje) {

    $ngConfirm({
      title: 'Te informamos:',
      content: mensaje,
      columnClass: 'small',
      type: 'orange',
      icon: 'fa fa-info-circle',
      theme: 'modern',
      typeAnimated: true,
      buttons: {
          tryAgain: {
              text: 'Inténtalo de nuevo',
              btnClass: 'btn-warning',
              action: function(){
              }
          },
          Cerrar: function () {
              text: 'Cerrar'
          }
      }
    });
  }


  function showPrompt(mensaje) {

    $ngConfirm({
      title: 'Te informamos:',
      content: mensaje,
      autoClose: 'logoutUser|10000',
      columnClass: 'small',
      type: 'orange',
      icon: 'fa fa-info-circle',
      theme: 'modern',
      typeAnimated: true,
      buttons: {
          logoutUser: {
              text: 'Aceptar',
              btnClass: 'btn-warning',
              action: function(){
                $window.location.href=URL_SITE;
              }
          }
      }
    });
  }

  $scope.validarDatos=function(objectToSend, formValid){
    if(formValid){
      if(!$scope.passMatchErrMsg){

        CodeService.changePass(objectToSend).then(function(data) {
            if(data.estado==1){
                showPrompt("Felicitaciones, acabas de registrar una nueva contraseña. Ahora si podrás ingresar a ver tus kms. <br />En unos segundos te enviaremos al Home page de Tampico Airlines.");
                $scope.user={};
            }else{
                showAlert("Lo sentimos, ha ocurrido un error. Verifica que no hayas usado este link para reespablecer tu password anteriormente");
            }
        });

      }else{
        showAlert("Las contraseñas no coinciden.");
      }
    }else{
      showAlert("Ingresa tu email y tu nueva contraseña de forma correcta");
    }
  }


  $scope.showModal = function(ev, template) {
    //console.log($scope.token_csrf);
    $mdDialog.show({
        controller: ['$scope', '$mdDialog', function($scope, $mdDialog) {
          // controller logic
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
        }],
        templateUrl: template,
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: $scope.customFullscreen
      })
      .then(function(answer) {
        $scope.status = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });

  };


  $scope.resetRegistro=function(){
    $scope.ganoKms=false;
    $scope.paso5 = false;
    $scope.paso1 = true;
    $scope.paso2 = false;
    $scope.paso2_2 = false;
    $scope.passMatchErrMsg = true;
    $scope.usuarioExistente=false;
    $scope.dataModificablePorUsuario={};
    $scope.user = {tipodocumento:'DNI'};
  }

  $scope.cpassValidate = function() {
    //equal to password or not?----------------------------------------
    if ($scope.user.password != $scope.user.cpassword) {
      $scope.passMatchErrMsg = true;
    } else {
      $scope.passMatchErrMsg = false;
    }
  }



  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }



});
