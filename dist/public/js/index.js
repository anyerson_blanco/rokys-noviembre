jQuery(document).ready(function($) {
    $actualWidth=$(window).width();

    if($actualWidth>=992){
      //$('#carouselPremios').removeClass('carousel');
    }

    //$(".menu-interno").fadeIn();

    $('select').niceSelect();

    $(".menu-interno__enlace, .menu-home__enlace1, .menu-home__enlace2, .menu-home__enlace3, .menu-home__enlace4, .linkMenu"  ).on('click', function(event) {
            var target = $(this).data("id");
            if($actualWidth>1100){
              if (target.length) {
                event.preventDefault();
                $.fn.fullpage.moveTo(target, 1);
              }
      			}else{
              if (target.length) {
                target = $("#A" + target);
                event.preventDefault();
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1000);
              }
              closeNav();
            }
    });



    //INICIO FUNCIONES PRELOADER
    // number of loaded images for preloader progress
    var loadedCount = 0; //current number of images loaded
    var imagesToLoad = $('.load').length; //number of slides with .bcg container
    var loadingProgress = 0; //timeline progress - starts at 0

    $('.section').imagesLoaded({
        background: true
    }).progress( function( instance, image ) {
        loadProgress();
    });

    function loadProgress(imgLoad, image)
    {
        //one more image has been loaded
        loadedCount++;
        loadingProgress = (loadedCount/imagesToLoad);
        // GSAP tween of our progress bar timeline
        TweenLite.to(progressTl, 0.7, {progress:loadingProgress, ease:Linear.easeNone});
    }

    //progress timeline
    var progressTl = new TimelineMax({
        paused: true,
        onUpdate: progressUpdate,
        onComplete: loadComplete
    });

    progressTl
        //tween the progress bar width
        .to($('.progress span'), 1, {width:100, ease:Linear.easeNone});

    //as the progress bar width updates and grows we put the percentage loaded in the screen
    function progressUpdate()
    {
        //the percentage loaded based on the tween's progress
        loadingProgress = Math.round(progressTl.progress() * 100);

        //we put the percentage in the screen
        $(".txt-perc").text(loadingProgress + '%');

    }

    function loadComplete() {

        // preloader out
        var preloaderOutTl = new TimelineMax();

        preloaderOutTl
            .to($('.progress'), 0.3, {y: 100, autoAlpha: 0, ease:Back.easeIn})
            .to($('.txt-perc'), 0.3, {y: 100, autoAlpha: 0, ease:Back.easeIn}, 0.1)
            .set($('body'), {className: '-=is-loading'})
            .set($('.wrapper-principal'), {className: '+=is-loaded'})
            .to($('#preloader'), 0.7, {yPercent: 100, ease:Power4.easeInOut})
            .set($('#preloader'), {className: '+=is-hidden'});

            new WOW().init();


        return preloaderOutTl;
    }

    //FIN FUNCIONES PRELOADER


   function fullpage(){

    $('#fullpage').fullpage({
      sectionsColor: ['#04417a'],
      anchors: ['Home'],
      menu: '.menu',
      navigation: true,
      scrollHorizontally: true,
      navigationPosition: 'left',
      navigationTooltips: ['Home'],
      continuousVertical: false,
      onLeave: function(index, nextIndex, direction) {

          /*if (nextIndex == 2 || nextIndex == 3 || nextIndex == 4 || nextIndex == 5  || nextIndex == 6) {
              $(".menu-interno").fadeIn();
          }else{
              $(".menu-interno").fadeOut();
          }*/

          if(nextIndex == 1){
            if ($('.wow').hasClass('animated')) {
              $(this).removeClass('animated');
              $(this).removeAttr('style');
              new WOW().init();
            }
          }


      },
      onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){

      }
    });
   }

  if ($(window).width() > 1100) {
  		fullpage();
		}

		$(window).on('orientationchange', function(event) {
			location.reload();
		});

    $(window).on('resize', function (){



			$newWidth=$(window).width();
			if($newWidth<=1100 && $actualWidth>1100){
					location.reload();
			}
			if($actualWidth<=1100){
				if($newWidth>1100){
					location.reload();
				}
			}
	});


});


function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    $(".linkMenu" ).delay(300).fadeIn( 400 );
    //document.getElementById("wrapper").style.marginLeft = "250px";
    $('.overlay3').show();
}


/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    //document.getElementById("wrapper").style.marginLeft = "0";
    $(".linkMenu" ).delay(0).fadeOut(0);
    $('.overlay3').hide();
}


var menu = document.getElementsByClassName("menu-interno__item");
var menu4 = document.getElementsByClassName("menu-interno__item--men4");

for (i = 0; i < menu.length; i++) {
  menu[i].addEventListener("click", function() {
    var panel = document.getElementsByClassName("panel");
    for (j = 0; j < panel.length; j++) {
      panel[j].style.animationDelay  = "0.6s";
      panel[j].style.animationDuration  = "1s";
    }
  });
}


let W = window.innerWidth;
let H = window.innerHeight;
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");
const maxConfettis = 60;
const particles = [];

const possibleColors = [
  "DodgerBlue",
  "OliveDrab",
  "Gold",
  "Pink",
  "SlateBlue",
  "LightBlue",
  "Gold",
  "Violet",
  "PaleGreen",
  "SteelBlue",
  "SandyBrown",
  "Chocolate",
  "Crimson"
];

function randomFromTo(from, to) {
  return Math.floor(Math.random() * (to - from + 1) + from);
}

function confettiParticle() {
  this.x = Math.random() * W; // x
  this.y = Math.random() * H - H; // y
  this.r = randomFromTo(11, 33); // radius
  this.d = Math.random() * maxConfettis + 11;
  this.color =
    possibleColors[Math.floor(Math.random() * possibleColors.length)];
  this.tilt = Math.floor(Math.random() * 33) - 11;
  this.tiltAngleIncremental = Math.random() * 0.07 + 0.05;
  this.tiltAngle = 0;

  this.draw = function() {
    context.beginPath();
    context.lineWidth = this.r / 2;
    context.strokeStyle = this.color;
    context.moveTo(this.x + this.tilt + this.r / 3, this.y);
    context.lineTo(this.x + this.tilt, this.y + this.tilt + this.r / 5);
    return context.stroke();
  };
}

function Draw() {
  const results = [];

  // Magical recursive functional love
  requestAnimationFrame(Draw);

  context.clearRect(0, 0, W, window.innerHeight);

  for (var i = 0; i < maxConfettis; i++) {
    results.push(particles[i].draw());
  }

  let particle = {};
  let remainingFlakes = 0;
  for (var i = 0; i < maxConfettis; i++) {
    particle = particles[i];

    particle.tiltAngle += particle.tiltAngleIncremental;
    particle.y += (Math.cos(particle.d) + 3 + particle.r / 2) / 2;
    particle.tilt = Math.sin(particle.tiltAngle - i / 3) * 15;

    if (particle.y <= H) remainingFlakes++;

    // If a confetti has fluttered out of view,
    // bring it back to above the viewport and let if re-fall.
    if (particle.x > W + 30 || particle.x < -30 || particle.y > H) {
      particle.x = Math.random() * W;
      particle.y = -30;
      particle.tilt = Math.floor(Math.random() * 10) - 20;
    }
  }

  return results;
}

window.addEventListener(
  "resize",
  function() {
    W = window.innerWidth;
    H = window.innerHeight;
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
  },
  false
);

// Push new confetti objects to `particles[]`
for (var i = 0; i < maxConfettis; i++) {
  particles.push(new confettiParticle());
}

// Initialize
canvas.width = W;
canvas.height = H;
Draw();
