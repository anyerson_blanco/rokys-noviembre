<?php

require_once __DIR__.'/../vendor/autoload.php';

Dotenv::load(__DIR__.'/../');

// Dotenv::load(__DIR__.'/../');

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->configure('jwt');

class_alias('Tymon\JWTAuth\Facades\JWTAuth', 'JWTAuth');
/** This gives you finer control over the payloads you create if you require it.
 *  Source: https://github.com/tymondesigns/jwt-auth/wiki/Installation
 */
class_alias('Tymon\JWTAuth\Facades\JWTFactory', 'JWTFactory'); // Optional

//class_alias('Tymon\JWTAuth\Facades\PayloadFactory', 'JWTFactory'); // Optional

$app->withEloquent();

$app->configure('services');

$app->configure('mail');



/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/



$app->singleton(
Illuminate\Contracts\Debug\ExceptionHandler::class,
App\Exceptions\Handler::class
);

$app->singleton(
Illuminate\Contracts\Console\Kernel::class,
App\Console\Kernel::class
);

if (!class_exists('Charts')) {
    class_alias('ConsoleTVs\Charts\Facades\Charts', 'Charts');
}

if (!class_exists('Excel')) {
    class_alias('Maatwebsite\Excel\Facades\Excel', 'Excel');
}

$app->register('Tymon\JWTAuth\Providers\JWTAuthServiceProvider'); // for request authentication
$app->register('ConsoleTVs\Charts\ChartsServiceProvider');
$app->register('Maatwebsite\Excel\ExcelServiceProvider');

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
//     // Illuminate\Cookie\Middleware\EncryptCookies::class,
//     // Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
      Illuminate\Session\Middleware\StartSession::class,
      Illuminate\View\Middleware\ShareErrorsFromSession::class,
      \App\Http\Middleware\ForceHttpProtocol::class,
//     // Laravel\Lumen\Http\Middleware\VerifyCsrfToken::class,
]);


$app->routeMiddleware([
    'api.auth.basic' => App\Http\Middleware\ApiSimpleAuth::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/



$app->register(App\Providers\AppServiceProvider::class);

$app->register(\Illuminate\Auth\Passwords\PasswordResetServiceProvider::class);

//$app->register(\Illuminate\Mail\MailServiceProvider::class);

// $app->register(App\Providers\EventServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../app/Http/routes.php';
});

return $app;
