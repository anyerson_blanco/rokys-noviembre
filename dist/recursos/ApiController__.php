<?php
class ApiController extends BaseController {

    public function showCodigo($codigo)
    {

        $credentials=["codigoacceso" => $codigo,
                "password" => "hsjkd356El2klls"];

        //estado: 3 (usuario inválido)
        //estado: 2 (usuario ya registrado como corredor)
        //estado: 1 (usuario ok!)

        if ( ! $token = JWTAuth::attempt($credentials) )
        {
            $estado = 3;
        }else{

            $countTienda = Codigo::where('codigoacceso', '=', $codigo)->where('tipo', '=', 'Tienda virtual')->count();
            if($countTienda>0){
              $countCodeVendido = DB::connection('mysqlVentas')->table('inscription_code')->where('State', '=', 3)->where('Active', '=', 1)->where('Code', '=',$codigo)->count();
              if($countCodeVendido>0){
                  $count = Corredor::where('codigoacceso', '=', $codigo)->count();
                  if($count>0){
                      $estado = 2;
                  }else{
                      $estado = 1;
                  }
              }else{
                $estado = 3;
              }
            }else{
              $count = Corredor::where('codigoacceso', '=', $codigo)->count();
              if($count>0){
                  $estado = 2;
              }else{
                  $estado = 1;
              }
            }

        }

        header('Content-Type: application/json');
        return Response::json(compact('token','estado'));

        /*$count = Codigo::where('codigoacceso', '=', $codigo)
            ->where('estado',"=",1)
            ->count();
        if($count>0){
            $data['estado'] = true;
        }else{
            $data['estado'] = false;
        }
        return Response::json($data);*/
    }


    public function showUser($token){
        var_dump(JWTAuth::login($token));
    }



    public function consumoNodni(){
        $credito =  new Credito;
        $credito->codigoacceso = Input::get('codigo');
        $credito->numdocumento = Input::get('dni');
        $credito->tipo = Input::get('tipo');
        $credito->save();
    }

    //cubillus:  esto ya no va
    /*public function verificarCodigoCorredor($codigo){
        $count = Corredor::where('codigoacceso', '=', $codigo)->count();
        if($count>0){
            $data['estado'] = true;
        }else{
            $data['estado'] = false;
        }
        return Response::json($data);
    }*/
    public function verificarFechaCarrera(){
        $fechaactual=date('Y-m-d');
        //$fechaactual='2014-09-30';
        $carrerafecha = Carrera::where('fechainiciodelivery', '<=', $fechaactual)
                        ->where('fechafindelivery','>=',$fechaactual)
                        ->count();
        if($carrerafecha>0){
            $data['estado'] = true;
        }else{
            $data['estado'] = false;
        }
        return Response::json($data);
        //return $carrerafecha;
    }
    public function verificarempleados(){
        $count = Corredor::where('idtipocorredor', '=', 2)->count();
        $cantidad = Carrera::where('cantidadempleados', '>', $count)
            ->count();
        if($cantidad>0){
            $data['estado'] = true;
        }else{
            $data['estado'] = false;
        }
        return Response::json($data);
    }

    //cubillus
    //nueva funcion para validar el dni del empleado

    public function verificarFecha(){
        $carrera = Carrera::where('idcarrera', '=', 1)->first();
        $maximo = $carrera->cantidaddelivery;
        $count = DB::table('corredor')
            ->select(DB::raw('count(*) as fechas'))
            ->where("tipodeentrega","=","D")
            ->count();
        if($maximo>$count){
            $data['estado'] = true;
        }else{
            $data['estado'] = false;
        }
        return Response::json($data);

    }

    //cubillus: funcion cambiada
    public function showCodigoMenores($codigo,$dni)
    {
        /*
        estado=1 (codigo de autorizacion correcto)
        estado=2 (codigo de autorizacion no existe)
        estado=3  (el dni de este usuario ya fue registrado)

        */
        //$codigo,$token
            $count = CodigoAutorizacion::where('codigoautorizacion', '=', $codigo)->count();
            if($count>0){
                $estado = 1;
                $count = Corredor::where('numdocumento', '=', $dni)->count();
                if($count>0){
                     $estado = 3;
                }
            }else{
                $estado = 2;
            }

        return Response::json(compact('estado'));
    }
    public function GrabarDatosPublic(){

        /*
        estado=1 (ok!)
        estado=2 (terminó el delivery)
        estado=3 (error en la transaccion)
        estado=4 (sesion expirada o intento de hackeo)
        estado=5 (el DNI ya ha sido registrado)
        */

		//descomentar solo cuando finalice la campaña

		//die("Fin de la inscripción.");

        if(!$this->compararToken(Input::get('Datacorredor1'),Input::get('DataToken'))){
            $mensaje['estado'] = 4;
            die(json_encode($mensaje));
        }

        $countDocumento = DB::table('corredor')
                    ->select("idcorredor")
                    ->where("numdocumento","=",Input::get('Datacorredor3'))
                    ->count();

        if($countDocumento>0){
            $mensaje['estado'] = 5;
            die(json_encode($mensaje));
        }


        $data=Input::get('DataDelivery');
        $correos = DB::table('envioscorreo')
            ->select(DB::raw('count(*)'))
            ->where("estado","=","1")
            ->count();


        if ( $correos == 0){
            $valor=1;
            DB::table('envioscorreo')
                ->where('id', $valor)
                ->update(array('estado' => 1));
        }else{
            if ($correos==10){
                $valor=1;
                DB::table('envioscorreo')
                    ->update(array('estado' => 0));
                DB::table('envioscorreo')
                    ->where('id', $valor)
                    ->update(array('estado' => 1));
            }else{
                $valor=$correos+1;
                DB::table('envioscorreo')
                    ->where('id', $valor)
                    ->update(array('estado' => 1));
            }
        }

        // Validar los campos
        $validator = Validator::make(
            array(
                'numdocumento' => Input::get('Datacorredor3'),
                'primernombre' => Input::get('Datacorredor6'),
                'apellidopaterno' => Input::get('Datacorredor4'),
                'correo' => Input::get('Datacorredor11'),
                'fechanacimiento' => Input::get('Datacorredor8'),
                'tallapolo' => Input::get('Datacorredor10'),
                'telefonofijo' => Input::get('Datacorredor12'),
                'telefonomovil' => Input::get('Datacorredor13'),
                'tipodeentrega' => Input::get('Datacorredor14')
            ),
            array(
                'numdocumento' => 'required',
                'primernombre' => 'required',
                'apellidopaterno' => 'required',
                'correo' => 'required|email',
                'fechanacimiento' => 'required|date',
                'tallapolo' => 'required',
                'telefonofijo' => 'required',
                'telefonomovil' => 'required',
                'tipodeentrega' => 'required'
            )
        );
        if ( $validator->fails() ) {
            $mensaje['estado'] = 3;
            die(json_encode($mensaje));
        }

        //si es delivery
        if ($data== "true"){
                $carrera = Carrera::where('idcarrera', '=', 1)->first();
                $maximo = $carrera->cantidaddelivery;
                $count = DB::table('corredor')
                    ->select(DB::raw('count(*) as fechas'))
                    ->where("tipodeentrega","=","D")->where("estado",1)
                    ->count();
                $fechaactual=date('Y-m-d');
                $carrerafecha = Carrera::where('fechainiciodelivery', '<=', $fechaactual)
                    ->where('fechafindelivery','>=',$fechaactual)
                    ->count();

                $verificarcodigo = Codigo::where('codigoacceso', '=', Input::get('Datacorredor1'))->where('estado',"=",1)->where('tipo', '=', 'Tienda virtual')->count();

                if($verificarcodigo>0){
                  $countCodeVendido = DB::connection('mysqlVentas')->table('inscription_code')->where('State', '=', 3)->where('Active', '=', 1)->where('Code', '=',Input::get('Datacorredor1'))->count();
                  if($countCodeVendido<=0){
                    $verificarcodigo=0;
                  }
                }else{
                  $verificarcodigo = Codigo::where('codigoacceso', '=', Input::get('Datacorredor1'))->where('estado',"=",1)->count();
                }


            if($maximo>$count and $carrerafecha>0 and $verificarcodigo>0){
                DB::beginTransaction();

                try {

                DB::select('SELECT numero FROM numpolo FOR UPDATE');
                DB::update('update numpolo set numero =LAST_INSERT_ID(numero+1)');
                $results = DB::select('SELECT LAST_INSERT_ID() as ultimo',array());
                $numerocorredor=$results[0]->ultimo;

                    $corredor = new Corredor;
                    $corredor-> idcarrera = 1;
                    $corredor-> idtipocorredor = 1;
                    $corredor-> codigoacceso = Input::get('Datacorredor1');
                    $corredor-> nrocorredor = $numerocorredor;
                    if (Input::get('Datacorredor2') == 1){
                        $corredor-> tipodocumento = 1;
                        $message = file_get_contents('mail_templates/delivery.html');
                    }
                    $corredor-> numdocumento = Input::get('Datacorredor3');
                    $corredor-> apellidopaterno = Input::get('Datacorredor4');
                    $corredor-> apellidomaterno = Input::get('Datacorredor5');
                    $corredor-> primernombre = Input::get('Datacorredor6');
                    $corredor-> segundonombre = Input::get('Datacorredor7');
                    $corredor-> edad = helpers::calculaedad(Input::get('Datacorredor8'));
                    $corredor-> fechanacimiento = Input::get('Datacorredor8');
                    if (Input::get('Datacorredor2') == 2){
                        $corredor->pais =Input::get('Datacorredor15');
                        $corredor-> tipodocumento = 2;
                        $message = file_get_contents('mail_templates/delivery-ce.html');
                    }
                    $corredor-> genero = Input::get('Datacorredor9');
                    $corredor-> tallapolo = Input::get('Datacorredor10');
                    $corredor-> correo = Input::get('Datacorredor11');
                    $corredor-> telefonofijo = Input::get('Datacorredor12');
                    $corredor-> telefonomovil = Input::get('Datacorredor13');
                    $corredor-> tipodeentrega = Input::get('Datacorredor14');
                    $corredor-> mailasociado = Input::get('Datacorredor20');

                    if (Input::get('Datacorredor2') == 3){
                        $corredor-> codigoautorizacion = Input::get('Datacorredor15');
                        $corredor-> tipodocumento = 1;
                        $message = file_get_contents('mail_templates/delivery.html');
                    }
                    $corredor->save();
                    $dataid =$corredor->id;
                    $delivery = new Delivery;
                    $delivery->telefonofijo =Input::get('DataDelivery1');
                    $delivery->telefonooficina =Input::get('DataDelivery2');
                    $delivery->anexo =Input::get('DataDelivery3');
                    $delivery->celular =Input::get('DataDelivery4');
                    $delivery->tipoubicacion =Input::get('DataDelivery5');
                    $delivery->direccion =Input::get('DataDelivery6');
                    $delivery->numero =Input::get('DataDelivery7');
                    $delivery->urbanizacion =Input::get('DataDelivery8');
                    $delivery->distrito =Input::get('DataDelivery9');
                    $delivery->referencia =Input::get('DataDelivery10');
                    $delivery->estadoentrega ="R";
                    $delivery->idcorredor=$dataid;
                    $delivery->save();


                } catch (ValidationException $e) {
                    $mensaje['estado'] = 3;
                    DB::rollback();
                    return Response::json($mensaje);
                        DB::rollback();
                        $mensaje['estado'] = 3;
                        return Response::json($mensaje);
                }
                DB::commit();

                try{

                    $direccionmail=Input::get('DataDelivery5') . ". " . Input::get('DataDelivery6') . " " . Input::get('DataDelivery7') . ", " . Input::get('DataDelivery9');

                    $nombrepemail = Input::get('Datacorredor6');
                    $nombresemail = Input::get('Datacorredor7');
                    $apellidopmail =Input::get('Datacorredor4');
                    $apellidosmail = Input::get('Datacorredor5');
                    $docemail =Input::get('Datacorredor3');
                    $message = str_replace('%nombrepemail%', $nombrepemail, $message);
                    $message = str_replace('%nombresemail%', $nombresemail, $message);
                    $message = str_replace('%apellidopmail%', $apellidopmail, $message);
                    $message = str_replace('%apellidosmail%', $apellidosmail, $message);
                    $message = str_replace('%docemail%', $docemail, $message);
                    $message = str_replace('%direccion%', $direccionmail, $message);

                    if (strpos(Input::get('Datacorredor11'), '@hotmail.com') !== false || strpos(Input::get('Datacorredor11'), '@outlook.com') !== false || strpos(Input::get('Datacorredor11'), '@outlook.es')!== false)
                    {
                      $this->sendViaApi("Confirmación de Inscripción – entrega delivery", $message ,Input::get('Datacorredor11'));
                    }else{
                      $this->sendViaPhpMailer("entel10.inscripcion".$valor."@entel10.pe","clavhgh356211","entel10.inscripcion".$valor."@entel10.pe","Confirmación de Inscripción – entrega delivery",$message,Input::get('Datacorredor11'),"inscripciones".$valor."@megashow.com.pe");
                    }

                    $clavecorredor =$corredor->id;
                    $correo = new Correo;
                    $correo ->idcorredor = $clavecorredor;
                    $correo ->estado = 1;
                    $correo ->save();

                } catch (\Exception $e) {

                }catch (\Exception $e) {
                }

                $mensaje['estado'] = 1;
                return Response::json($mensaje);
            }else{
                //se acabo el delivery
                $mensaje['estado'] = 2;
                return Response::json($mensaje);
            }
            //si no es delivery (MODULO)
        }else{
            $verificarcodigoexiste = Corredor::where('codigoacceso', '=', strtoupper(Input::get('Datacorredor1')))->count();
            $verificardni = Corredor::where('numdocumento', '=', Input::get('Datacorredor3'))->count();

            $verificarcodigo = Codigo::where('codigoacceso', '=', Input::get('Datacorredor1'))->where('estado',"=",1)->where('tipo', '=', 'Tienda virtual')->count();

            if($verificarcodigo>0){
              $countCodeVendido = DB::connection('mysqlVentas')->table('inscription_code')->where('State', '=', 3)->where('Active', '=', 1)->where('Code', '=',Input::get('Datacorredor1'))->count();
              if($countCodeVendido<=0){
                $verificarcodigo=0;
              }
            }else{
              $verificarcodigo = Codigo::where('codigoacceso', '=', Input::get('Datacorredor1'))->where('estado',"=",1)->count();
            }

            if ($verificarcodigo >0  ){

                DB::beginTransaction();

                try {

                DB::select('SELECT numero FROM numpolo FOR UPDATE');
                DB::update('update numpolo set numero =LAST_INSERT_ID(numero+1)');
                $results = DB::select('SELECT LAST_INSERT_ID() as ultimo',array());
                $numerocorredor=$results[0]->ultimo;

                $corredor = new Corredor;
                $corredor-> idcarrera = 1;
                $corredor-> idtipocorredor = 1;
                $corredor-> codigoacceso = Input::get('Datacorredor1');
                $corredor-> nrocorredor = $numerocorredor;
                if (Input::get('Datacorredor2') == 1){
                    $corredor-> tipodocumento = 1;
                    $message = file_get_contents('mail_templates/modulo.html');
                }
                $corredor-> numdocumento = Input::get('Datacorredor3');
                $corredor-> apellidopaterno = Input::get('Datacorredor4');
                $corredor-> apellidomaterno = Input::get('Datacorredor5');
                $corredor-> primernombre = Input::get('Datacorredor6');
                $corredor-> segundonombre = Input::get('Datacorredor7');
                $corredor-> edad = helpers::calculaedad(Input::get('Datacorredor8'));
                $corredor-> fechanacimiento = Input::get('Datacorredor8');
                if (Input::get('Datacorredor2') == 2){
                    $corredor-> pais = Input::get('Datacorredor15');
                    $corredor-> tipodocumento = 2;
                    $message = file_get_contents('mail_templates/modulo-ce.html');
                }
                $corredor-> genero = Input::get('Datacorredor9');
                $corredor-> tallapolo = Input::get('Datacorredor10');
                $corredor-> correo = Input::get('Datacorredor11');
                $corredor-> telefonofijo = Input::get('Datacorredor12');
                $corredor-> telefonomovil = Input::get('Datacorredor13');
                $corredor-> tipodeentrega = Input::get('Datacorredor14');
                if (Input::get('Datacorredor2') == 3){
                     $corredor-> codigoautorizacion = Input::get('Datacorredor15');
                     $corredor-> tipodocumento = 1;
                     $message = file_get_contents('mail_templates/modulo.html');
                }
                $corredor-> mailasociado = Input::get('Datacorredor20');
                $corredor->save();

                } catch (ValidationException $e) {
                    $mensaje['estado'] = 3;
                    DB::rollback();
                    return Response::json($mensaje);
                } catch (\Exception $e) {
                        DB::rollback();
                        $mensaje['estado'] = 3;
                        return Response::json($mensaje);
                }
                DB::commit();

                try{
                  $clavecorredor2 =$corredor->id;
                  $correo = new Correo;
                  $correo ->idcorredor = $clavecorredor2;
                  $correo ->estado = 1;
                  $correo ->save();

                  $nombrepemail = Input::get('Datacorredor6');
                  $nombresemail = Input::get('Datacorredor7');
                  $apellidopmail =Input::get('Datacorredor4');
                  $apellidosmail = Input::get('Datacorredor5');
                  $docemail =Input::get('Datacorredor3');

                  $message = str_replace('%nombrepemail%', $nombrepemail, $message);
                  $message = str_replace('%nombresemail%', $nombresemail, $message);
                  $message = str_replace('%apellidopmail%', $apellidopmail, $message);
                  $message = str_replace('%apellidosmail%', $apellidosmail, $message);
                  $message = str_replace('%docemail%', $docemail, $message);

                  if (strpos(Input::get('Datacorredor11'), '@hotmail.com') !== false || strpos(Input::get('Datacorredor11'), '@outlook.com') !== false || strpos(Input::get('Datacorredor11'), '@outlook.es')!== false)
                  {
                    $this->sendViaApi("Confirmación de Inscripción – entrega en módulo", $message ,Input::get('Datacorredor11'));
                  }else{
                    $this->sendViaPhpMailer("entel10.inscripcion".$valor."@entel10.pe","clavhgh356211","entel10.inscripcion".$valor."@entel10.pe","Confirmación de Inscripción – entrega en módulo",$message,Input::get('Datacorredor11'),"inscripciones".$valor."@megashow.com.pe");
                  }
                }catch (\Exception $e) {

                }catch (\Exception $e) {
                }

                $mensaje['estado'] = 1;
                return Response::json($mensaje);
            }else{
                $mensaje['estado'] = 4;
                return Response::json($mensaje);
            }
        }

    }
    public function GrabarDatosEmpleados(){
        $data=Input::get('DataDelivery');
        $count = Corredor::where('idtipocorredor', '=', 2)->count();
        $cantidad = Carrera::where('cantidadempleados', '>', $count)
            ->count();
        $correos = DB::table('envioscorreo')
            ->select(DB::raw('count(*)'))
            ->where("estado","=","1")
            ->count();

        if ( $correos == 0){
            $valor=1;
            DB::table('envioscorreo')
                ->where('id', $valor)
                ->update(array('estado' => 1));
        }else{
            if ($correos==10){
                $valor=1;
                DB::table('envioscorreo')
                    ->update(array('estado' => 0));
                DB::table('envioscorreo')
                    ->where('id', $valor)
                    ->update(array('estado' => 1));
            }else{
                $valor=$correos+1;
                DB::table('envioscorreo')
                    ->where('id', $valor)
                    ->update(array('estado' => 1));
            }
        }

        if($cantidad>0){
                if ($data== "true"){

                            $carrera = Carrera::where('idcarrera', '=', 1)->first();
                            $maximo = $carrera->cantidaddelivery;
                            $count = DB::table('corredor')
                            ->select(DB::raw('count(*) as fechas'))
                            ->where("tipodeentrega","=","D")->where("estado",1)
                            ->count();
                            $fechaactual=date('Y-m-d');
                            $carrerafecha = Carrera::where('fechainiciodelivery', '<=', $fechaactual)
                            ->where('fechafindelivery','>=',$fechaactual)
                            ->count();
                            $verificarempleado = Empleado::where('nrodocumento', '=', Input::get('Datacorredor3'))->count();

                        if($maximo>$count and $carrerafecha>0 and $verificarempleado>0){

                        DB::beginTransaction();

                        try {
                        DB::select('SELECT numero FROM numpolo FOR UPDATE');
                        DB::update('update numpolo set numero =LAST_INSERT_ID(numero+1)');
                        $results = DB::select('SELECT LAST_INSERT_ID() as ultimo',array());
                        $numerocorredor=$results[0]->ultimo;

                            $corredor = new Corredor;
                            $corredor-> idcarrera = 1;
                            $corredor-> idtipocorredor = 2;
                            $corredor-> codigoacceso = Input::get('Datacorredor1');
                            $corredor-> nrocorredor = $numerocorredor;
                            $corredor-> numdocumento = Input::get('Datacorredor3');
                            $corredor-> apellidopaterno = Input::get('Datacorredor4');
                            $corredor-> apellidomaterno = Input::get('Datacorredor5');
                            $corredor-> primernombre = Input::get('Datacorredor6');
                            $corredor-> segundonombre = Input::get('Datacorredor7');
                            $corredor-> edad = helpers::calculaedad(Input::get('Datacorredor8'));
                            $corredor-> fechanacimiento = Input::get('Datacorredor8');
                            if (Input::get('Datacorredor2') == 1){
                                $corredor-> tipodocumento = 1;
                                $message = file_get_contents('mail_templates/delivery.html');
                            }
                            $corredor-> genero = Input::get('Datacorredor9');
                            $corredor-> tallapolo = Input::get('Datacorredor10');
                            $corredor-> correo = Input::get('Datacorredor11');
                            $corredor-> telefonofijo = Input::get('Datacorredor12');
                            $corredor-> telefonomovil = Input::get('Datacorredor13');
                            $corredor-> tipodeentrega = Input::get('Datacorredor14');
                            $corredor-> mailasociado = Input::get('Datacorredor20');
                            //$corredor->delivery_iddelivery=$dataid;
                            if (Input::get('Datacorredor2') == 2){
                                $corredor-> pais = Input::get('Datacorredor15');
                                $corredor-> tipodocumento = 2;
                                $message = file_get_contents('mail_templates/delivery-ce.html');
                            }
                            $corredor->save();
                            $dataid =$corredor->id;

                            $delivery = new Delivery;
                            $delivery->telefonofijo =Input::get('DataDelivery1');
                            $delivery->telefonooficina =Input::get('DataDelivery2');
                            $delivery->anexo =Input::get('DataDelivery3');
                            $delivery->celular =Input::get('DataDelivery4');
                            $delivery->tipoubicacion =Input::get('DataDelivery5');
                            $delivery->direccion =Input::get('DataDelivery6');
                            $delivery->numero =Input::get('DataDelivery7');
                            $delivery->urbanizacion =Input::get('DataDelivery8');
                            $delivery->distrito =Input::get('DataDelivery9');
                            $delivery->referencia =Input::get('DataDelivery10');
                            $delivery->estadoentrega ="R";
                            $delivery->idcorredor =$dataid;
                            $delivery->save();

                            $clavecorredor =$corredor->id;
                            $correo = new Correo;
                            $correo ->idcorredor = $clavecorredor;
                            $correo ->estado = 1;
                            $correo ->save();
                         } catch (ValidationException $e) {
                            $mensaje['estado'] = false;
                            DB::rollback();
                            return Response::json($mensaje);
                        } catch (\Exception $e) {
                            DB::rollback();
                            $mensaje['estado'] = false;
                            return Response::json($mensaje);
                        }
                        DB::commit();

                        try {
                            $nombrepemail = Input::get('Datacorredor6');
                            $nombresemail = Input::get('Datacorredor7');
                            $apellidopmail =Input::get('Datacorredor4');
                            $apellidosmail = Input::get('Datacorredor5');

                            //validar direccion
                            $direccionmail=Input::get('DataDelivery5') . ". " . Input::get('DataDelivery6') . " " . Input::get('DataDelivery7') . ", " . Input::get('DataDelivery9');

                            $docemail =Input::get('Datacorredor3');
                            $message = str_replace('%nombrepemail%', $nombrepemail, $message);
                            $message = str_replace('%nombresemail%', $nombresemail, $message);
                            $message = str_replace('%apellidopmail%', $apellidopmail, $message);
                            $message = str_replace('%apellidosmail%', $apellidosmail, $message);
                            $message = str_replace('%docemail%', $docemail, $message);
                            $message = str_replace('%direccion%', $direccionmail, $message);


                            require 'class.phpmailer.php';
                            require 'class.smtp.php';
                            $mail = new PHPMailer();
                            $mail->IsHTML(true); // si es html o txt
                            $mail->CharSet = 'UTF-8';
                            $mail->IsSMTP();
                            $mail->Host = "162.144.254.186";//smpt de nuestro correo
                            $mail->SMTPAuth = true; //por si necesita auentificación
                            $mail->Username ="entel10.inscripcion".$valor."@entel10.pe";
                            $mail->Password = "clavhgh356211";
                            $mail->From = "entel10.inscripcion".$valor."@entel10.pe";
                            $mail->FromName = "Entel 10, la carrera que dura 10 años";
                            $mail->Subject = "Confirmación de Inscripción – entrega delivery.";
                            $mail->AddAddress(Input::get('Datacorredor11'));//el email al que vá
                            $mail->AddBCC("inscripciones".$valor."@megashow.com.pe");
                            $mail->MsgHTML($message);
                            //$mail->Body = $body;//cogemos el cuerpo completo
                            $mail->Send();
                        } catch (\Exception $e) {
                        }

                            $datos['estado'] = true;
                            return Response::json($datos);
                        }else{
                            $datos['estado'] = false;
                            return Response::json($datos);
                        }
                }else{
                    $verificarempleado = Empleado::where('nrodocumento', '=', Input::get('Datacorredor3'))->count();
                    if ($verificarempleado>0){

                        DB::beginTransaction();

                        try {
                        DB::select('SELECT numero FROM numpolo FOR UPDATE');
                        DB::update('update numpolo set numero =LAST_INSERT_ID(numero+1)');
                        $results = DB::select('SELECT LAST_INSERT_ID() as ultimo',array());
                        $numerocorredor=$results[0]->ultimo;

                        $corredor = new Corredor;
                        $corredor-> idcarrera = 1;
                        $corredor-> idtipocorredor = 2;
                        $corredor-> nrocorredor = $numerocorredor;
                        $corredor-> codigoacceso = Input::get('Datacorredor1');
                        $corredor-> numdocumento = Input::get('Datacorredor3');
                        $corredor-> apellidopaterno = Input::get('Datacorredor4');
                        $corredor-> apellidomaterno = Input::get('Datacorredor5');
                        $corredor-> primernombre = Input::get('Datacorredor6');
                        $corredor-> segundonombre = Input::get('Datacorredor7');
                        $corredor-> edad = helpers::calculaedad(Input::get('Datacorredor8'));
                        $corredor-> fechanacimiento = Input::get('Datacorredor8');
                        if (Input::get('Datacorredor2') == 1){
                            $corredor-> tipodocumento = 1;
                            $message = file_get_contents('mail_templates/modulo.html');
                        }
                        if (Input::get('Datacorredor2') == 2){
                            $corredor-> pais = Input::get('Datacorredor15');
                            $corredor-> tipodocumento = 2;
                            $message = file_get_contents('mail_templates/modulo-ce.html');
                        }
                        $corredor-> genero = Input::get('Datacorredor9');
                        $corredor-> tallapolo = Input::get('Datacorredor10');
                        $corredor-> correo = Input::get('Datacorredor11');
                        $corredor-> telefonofijo = Input::get('Datacorredor12');
                        $corredor-> telefonomovil = Input::get('Datacorredor13');
                        $corredor-> tipodeentrega = Input::get('Datacorredor14');
                        $corredor-> mailasociado = Input::get('Datacorredor20');
                        $corredor->save();

                        $clavecorredor2 =$corredor->id;
                        $correo = new Correo;
                        $correo ->idcorredor = $clavecorredor2;
                        $correo ->estado = 1;
                        $correo ->save();

                        $nombrepemail = Input::get('Datacorredor6');
                        $nombresemail = Input::get('Datacorredor7');
                        $apellidopmail =Input::get('Datacorredor4');
                        $apellidosmail = Input::get('Datacorredor5');
                        $docemail =Input::get('Datacorredor3');

                        $message = str_replace('%nombrepemail%', $nombrepemail, $message);
                        $message = str_replace('%nombresemail%', $nombresemail, $message);
                        $message = str_replace('%apellidopmail%', $apellidopmail, $message);
                        $message = str_replace('%apellidosmail%', $apellidosmail, $message);
                        $message = str_replace('%docemail%', $docemail, $message);

                        require 'class.phpmailer.php';
                        require 'class.smtp.php';
                        $mail = new PHPMailer();

                        $mail->IsHTML(true); // si es html o txt
                        $mail->CharSet = 'UTF-8';
                        $mail->IsSMTP();
                        $mail->Host = "162.144.254.186";//smpt de nuestro correo
                        $mail->SMTPAuth = true; //por si necesita auentificación
                        $mail->Username = "entel10.inscripcion".$valor."@entel10.pe";
                        $mail->Password = "clavhgh356211";
                        $mail->From = "entel10.inscripcion".$valor."@entel10.pe";
                        $mail->FromName = "Entel 10, la carrera que dura 10 años";
                        $mail->Subject = "Confirmación de Inscripción – entrega en modulo.";
                        $mail->AddAddress(Input::get('Datacorredor11'));//el email al que vá
                        $mail->AddBCC("inscripciones".$valor."@megashow.com.pe");
                        $mail->MsgHTML($message);
                        //$mail->Body = $body;//cogemos el cuerpo completo
                        $mail->Send();


                        } catch (ValidationException $e) {
                            $mensaje['estado'] = false;
                            DB::rollback();
                            return Response::json($mensaje);
                        } catch (\Exception $e) {
                            DB::rollback();
                            $mensaje['estado'] = false;
                            return Response::json($mensaje);
                        }
                        DB::commit();

                        $datos['estado'] = true;
                        //$datos['estado'] = false;
                        return Response::json($datos);

                    }else{
                        $datos['estado'] = false;
                         return Response::json($datos);
                    }

                }
        }else {
            $datos['estado'] = false;
             return Response::json($datos);
        }


    }
    public function GrabarDatosProfesionales(){
        $data=Input::get('DataDelivery');
        if ($data== "true"){
                        $carrera = Carrera::where('idcarrera', '=', 1)->first();
                        $maximo = $carrera->cantidaddelivery;
                        $count = DB::table('corredor')
                            ->select(DB::raw('count(*) as fechas'))
                            ->where("tipodeentrega","=","D")->where("estado",1)
                            ->count();
                        $fechaactual=date('Y-m-d');
                        $carrerafecha = Carrera::where('fechainiciodelivery', '<=', $fechaactual)
                            ->where('fechafindelivery','>=',$fechaactual)
                            ->count();
                if($maximo>$count and $carrerafecha>0){

                DB::beginTransaction();

                try {
                        DB::select('SELECT numero FROM numpolo FOR UPDATE');
                        DB::update('update numpolo set numero =LAST_INSERT_ID(numero+1)');
                        $results = DB::select('SELECT LAST_INSERT_ID() as ultimo',array());
                        $numerocorredor=$results[0]->ultimo;

                $corredor = new Corredor;
                $corredor-> idcarrera = 1;
                $corredor-> idtipocorredor = 3;
                $corredor-> codigoacceso = Input::get('Datacorredor1');
                $corredor-> nrocorredor = $numerocorredor;
                $corredor-> numdocumento = Input::get('Datacorredor3');
                $corredor-> apellidopaterno = Input::get('Datacorredor4');
                $corredor-> apellidomaterno = Input::get('Datacorredor5');
                $corredor-> primernombre = Input::get('Datacorredor6');
                $corredor-> segundonombre = Input::get('Datacorredor7');
                $corredor-> edad = helpers::calculaedad(Input::get('Datacorredor8'));
                $corredor-> fechanacimiento = Input::get('Datacorredor8');
                if (Input::get('Datacorredor2') == 1){
                    $corredor-> tipodocumento = 1;
                    $message = file_get_contents('mail_templates/delivery.html');
                }
                if (Input::get('Datacorredor2') == 2){
                    $corredor-> pais = Input::get('Datacorredor15');
                    $corredor-> tipodocumento = 2;
                    $message = file_get_contents('mail_templates/delivery-ce.html');
                }

                $corredor-> genero = Input::get('Datacorredor9');
                $corredor-> tallapolo = Input::get('Datacorredor10');
                $corredor-> correo = Input::get('Datacorredor11');
                $corredor-> telefonofijo = Input::get('Datacorredor12');
                $corredor-> telefonomovil = Input::get('Datacorredor13');
                $corredor-> tipodeentrega = Input::get('Datacorredor14');
                $corredor-> mailasociado = Input::get('Datacorredor20');
               // $corredor->delivery_iddelivery=$dataid;
                $corredor->save();
                $clavecorredor =$corredor->id;

                $delivery = new Delivery;
                $delivery->telefonofijo =Input::get('DataDelivery1');
                $delivery->telefonooficina =Input::get('DataDelivery2');
                $delivery->anexo =Input::get('DataDelivery3');
                $delivery->celular =Input::get('DataDelivery4');
                $delivery->tipoubicacion =Input::get('DataDelivery5');
                $delivery->direccion =Input::get('DataDelivery6');
                $delivery->numero =Input::get('DataDelivery7');
                $delivery->urbanizacion =Input::get('DataDelivery8');
                $delivery->distrito =Input::get('DataDelivery9');
                $delivery->referencia =Input::get('DataDelivery10');
                $delivery->estadoentrega ="R";
                $delivery->idcorredor =$clavecorredor;
                $delivery->save();

                $correo = new Correo;
                $correo ->idcorredor = $clavecorredor;
                $correo ->estado = 1;
                $correo ->save();

                } catch (ValidationException $e) {
                            $mensaje['estado'] = false;
                            DB::rollback();
                            return Response::json($mensaje);
                } catch (\Exception $e) {
                            DB::rollback();
                            $mensaje['estado'] = false;
                            return Response::json($mensaje);
                }
                DB::commit();

                try {
                    $nombrepemail = Input::get('Datacorredor6');
                    $nombresemail = Input::get('Datacorredor7');
                    $apellidopmail =Input::get('Datacorredor4');
                    $apellidosmail = Input::get('Datacorredor5');
                    $docemail =Input::get('Datacorredor3');
                    $message = str_replace('%nombrepemail%', $nombrepemail, $message);
                    $message = str_replace('%nombresemail%', $nombresemail, $message);
                    $message = str_replace('%apellidopmail%', $apellidopmail, $message);
                    $message = str_replace('%apellidosmail%', $apellidosmail, $message);
                    $message = str_replace('%docemail%', $docemail, $message);
                    require 'class.phpmailer.php';
                    require 'class.smtp.php';
                    $mail = new PHPMailer();
                    $mail->IsHTML(true); // si es html o txt
                    $mail->CharSet = 'UTF-8';
                    $mail->IsSMTP();
                    $mail->Host = "162.144.254.186";//smpt de nuestro correo
                    $mail->SMTPAuth = true; //por si necesita auentificación
                    $mail->Username ="entel10.inscripcion".rand(1, 10)."@entel10.pe";
                    $mail->Password = "clavhgh356211";
                    $mail->From = "entel10.inscripcion".rand(1, 10)."@entel10.pe";
                    $mail->FromName = "Entel 10, la carrera que dura 10 años";
                    $mail->Subject = "Confirmación de Inscripción – entrega delivery";
                    $mail->AddAddress(Input::get('Datacorredor11'));//el email al que vá
                    $mail->AddBCC("inscripciones".rand(1, 10)."@megashow.com.pe");
                    $mail->MsgHTML($message);
                    $mail->Send();
                } catch (\Exception $e) {
                }
                    $mensaje['estado'] = true;
                    return Response::json($mensaje);

                }else{
                     $mensaje['estado'] = false;
                    return Response::json($mensaje);
                }

        }else{
             DB::beginTransaction();

                try {
                        DB::select('SELECT numero FROM numpolo FOR UPDATE');
                        DB::update('update numpolo set numero =LAST_INSERT_ID(numero+1)');
                        $results = DB::select('SELECT LAST_INSERT_ID() as ultimo',array());
                        $numerocorredor=$results[0]->ultimo;

                        $corredor = new Corredor;
                        $corredor-> idcarrera = 1;
                        $corredor-> idtipocorredor = 3;
                        $corredor-> nrocorredor = $numerocorredor;
                        $corredor-> codigoacceso = Input::get('Datacorredor1');
                        $corredor-> numdocumento = Input::get('Datacorredor3');
                        $corredor-> apellidopaterno = Input::get('Datacorredor4');
                        $corredor-> apellidomaterno = Input::get('Datacorredor5');
                        $corredor-> primernombre = Input::get('Datacorredor6');
                        $corredor-> segundonombre = Input::get('Datacorredor7');
                        $corredor-> edad = helpers::calculaedad(Input::get('Datacorredor8'));
                        $corredor-> fechanacimiento = Input::get('Datacorredor8');
                        if (Input::get('Datacorredor2') == 1){
                            $corredor-> tipodocumento = 1;
                            $message = file_get_contents('mail_templates/modulo.html');
                        }
                        if (Input::get('Datacorredor2') == 2){
                            $corredor-> pais = Input::get('Datacorredor15');
                            $corredor-> tipodocumento = 2;
                            $message = file_get_contents('mail_templates/modulo-ce.html');
                        }
                        $corredor-> genero = Input::get('Datacorredor9');
                        $corredor-> tallapolo = Input::get('Datacorredor10');
                        $corredor-> correo = Input::get('Datacorredor11');
                        $corredor-> telefonofijo = Input::get('Datacorredor12');
                        $corredor-> telefonomovil = Input::get('Datacorredor13');
                        $corredor-> tipodeentrega = Input::get('Datacorredor14');
                        $corredor-> mailasociado = Input::get('Datacorredor20');

                        $corredor->save();
                        $clavecorredor2 =$corredor->id;
                        $correo = new Correo;
                        $correo ->idcorredor = $clavecorredor2;
                        $correo ->estado = 1;
                        $correo ->save();
                } catch (ValidationException $e) {
                        $mensaje['estado'] = false;
                        DB::rollback();
                        return Response::json($mensaje);
                } catch (\Exception $e) {
                        DB::rollback();
                        $mensaje['estado'] = false;
                        return Response::json($mensaje);
                }
                DB::commit();
                        $nombrepemail = Input::get('Datacorredor6');
                        $nombresemail = Input::get('Datacorredor7');
                        $apellidopmail =Input::get('Datacorredor4');
                        $apellidosmail = Input::get('Datacorredor5');
                        $docemail =Input::get('Datacorredor3');

                        $message = str_replace('%nombrepemail%', $nombrepemail, $message);
                        $message = str_replace('%nombresemail%', $nombresemail, $message);
                        $message = str_replace('%apellidopmail%', $apellidopmail, $message);
                        $message = str_replace('%apellidosmail%', $apellidosmail, $message);
                        $message = str_replace('%docemail%', $docemail, $message);

                        require 'class.phpmailer.php';
                        require 'class.smtp.php';
                        $mail = new PHPMailer();

                        $mail->IsHTML(true); // si es html o txt
                        $mail->CharSet = 'UTF-8';
                        $mail->IsSMTP();
                        $mail->Host = "162.144.254.186";//smpt de nuestro correo
                        $mail->SMTPAuth = true; //por si necesita auentificación
                        $mail->Username = "entel10.inscripcion".rand(1, 10)."@entel10.pe";
                        $mail->Password = "clavhgh356211";
                        $mail->From = "entel10.inscripcion".rand(1, 10)."@entel10.pe";
                        $mail->FromName = "Entel 10, la carrera que dura 10 años";
                        $mail->Subject = "Confirmación de Inscripción – entrega en módulo";
                        $mail->AddAddress(Input::get('Datacorredor11'));//el email al que vá
                        $mail->AddBCC("inscripciones".rand(1, 10)."@megashow.com.pe");
                        $mail->MsgHTML($message);
                        //$mail->Body = $body;//cogemos el cuerpo completo
                        $mail->Send();
                        $mensaje['estado'] = true;
                        return Response::json($mensaje);
        }

    }


    private function compararToken($codigo,$token){

        $user = JWTAuth::login($token);

        //validar que el codigo enviado sea el mismo del token
        if($codigo==$user->codigoacceso){
            return true;
        }else{
            return false;
        }

    }

    private function consumoCredito($codigo,$dni,$ip){
        $credito =  new Credito;
        $credito->codigoacceso = $codigo;
        $credito->numdocumento = $dni;
        $credito->ip = $ip;
        $credito->save();
    }


    private function sendViaPhpMailer($username,$password,$from,$subject,$message,$email,$bcc){

      require 'class.phpmailer.php';
      require 'class.smtp.php';
      $mail = new PHPMailer();

      $mail->IsHTML(true);
      $mail->CharSet = 'UTF-8';
      $mail->IsSMTP();
      $mail->Host = "162.144.254.186";
      $mail->SMTPAuth = true;
      $mail->Username = $username;
      $mail->Password = $password;
      $mail->From = $from;
      $mail->FromName = "Entel 10, la carrera que dura 10 años";
      $mail->Subject = $subject;
      $mail->AddAddress($email);
      $mail->AddBCC($bcc);
      $mail->MsgHTML($message);

      $mail->Send();

    }

    private function sendViaApi($subject,$message,$email){

      require 'SmtpApi.php';

$sPubKey = '
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCwhEXLRTHGseWJctQ0F8NU4DIV
Wh0+Em4/R9XQaDUujx2+rmY2h/vET2VD144y6hliOGyxQSpzF7ZE2rBWLg0GGWao
S5dKwDwnCmL3x4jXfvXVO3nKl+edNda5WwT++Joapstoyg+0aIS37Nnz9Pn+G7ow
iCFmvmw1sjofaMlK6wIDAQAB
-----END PUBLIC KEY-----
';

        $oApi = new SmtpApi($sPubKey);
        $FromName = "Entel 10, la carrera que dura 10 años";
        $bcc="inscripciones".rand(1, 10)."@megashow.com.pe";

        $aEmail = array(
            'html' => $message,
            'text' => '',
            'encoding' => 'UTF-8',
            'subject' => $subject,
            'from' => array(
                'name' => $FromName,
                'email' => 'entel.venta-online@entel10.pe',
            ),
            'to' => array(
                array(
                    'email' => $email
                )
            ),'bcc' => array(
                array(
                    'email' => $bcc
                )
            ),
        );

        $res = $oApi->send_email($aEmail);

    }
}
