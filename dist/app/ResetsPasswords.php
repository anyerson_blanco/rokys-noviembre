<?php
namespace App;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str as Str;
use DB;
use Carbon\Carbon;
use Mail;

trait ResetsPasswords
{
    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getEmail()
    {
        //logica que recibe el token y enalua si existe en la BD, si no existe se redirecciona hacia el HOME, si exixte se muestra la vista

        return view('auth.password');
    }


    public function postEmail(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }
    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetLinkEmail(Request $request)
    {
      $this->validate($request, ['email' => 'required|email']);

      $user = DB::table('users')->where('email', $request->email)->where('estado', 'A')->first();

      if (is_null($user)) {
        $data['estado'] = 0;

      }else{
        $token = hash_hmac('sha256', Str::random(40), env('APP_KEY'));
         DB::table('password_resets')->insert( ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()] );

         Mail::send('emails.password', ['token' => $token], function ($m) use ($user) {
   					$m->from('info@tampicoairlines.com', 'Tampico Airlines');

   					$m->to($user->email, $user->nombre1 . " " . $user->apellido1 )->subject('Recupera tu contraseña - Tampico Airlines');
   			});

        $data['estado'] = 1;


      }

      return $data;

        /*$broker = $this->getBroker();
        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);
            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }*/
    }
    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }
    /**
     * Get the response for after the reset link has been successfully sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        return response()->json(['success' => true]);
    }
    /**
     * Get the response for after the reset link could not be sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailFailureResponse($response)
    {
      return response()->json(['success' => false]);
    }
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        return $this->reset($request);
    }
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules());
        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
        //var_dump($credentials);
        $reset = DB::table('password_resets')->where('email', $credentials['email'])->where('token', $credentials['token'])->first();

        if (!is_null($reset)) {
          if($credentials['password']==$credentials['password_confirmation']){
              $pass = bcrypt($credentials['password']);
              DB::update("update users set password = '". $pass ."' where email='". $credentials['email'] ."'");
              DB::table('password_resets')->where('email', '=', $credentials['email'])->delete();
              $data['estado'] = 1;

          }
        }else{
          $data['estado'] = 0;

        }

        return $data;

        /*$broker = $this->getBroker();
        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);
            default:
                return $this->getResetFailureResponse($request, $response);
        }*/
    }
    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function getResetValidationRules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
        return response()->json(['success' => true]);
    }
    /**
     * Get the response for after a successful password reset.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse()
    {
        return response()->json(['success' => true]);
    }
    /**
     * Get the response for after a failing password reset.
     *
     * @param  Request  $request
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetFailureResponse(Request $request, $response)
    {
        return response()->json(['success' => false]);
    }
    /**
     * Get the broker to be used during password reset.
     *
     * @return string|null
     */
    public function getBroker()
    {
        return property_exists($this, 'broker') ? $this->broker : null;
    }


    public function getReset($token = null)
  	{
  		if (is_null($token))
  		{
  			throw new NotFoundHttpException;
  		} else {
        $reset = DB::table('password_resets')->where('token', $token)->get();
        if(!$reset) return redirect('/');
      }

  		return view('auth.reset')->with('token', $token);
  	}
}
