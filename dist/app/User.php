<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
    use Authenticatable;
    use CanResetPassword;

    protected $table = 'users';


    /*$table->increments('id');
    $table->string('nombres', 80);
    $table->string('apellidos', 80);
    $table->string('num_documento', 20);
    $table->string('telefono', 12);
    $table->string('email', 40);
    $table->string('distribuidor', 250);
    $table->string('motor', 30);
    $table->string('chasis', 30);
    $table->string('combustible', 10);
    $table->string('fechacompra', 20);
    $table->string('departamento', 40);
    $table->string('provincia', 40);
    $table->string('distrito', 40);*/
    static $rules= [
      'nombres' => 'required',
      'apellidos' => 'required',
      'num_documento' => 'required',
      'telefono' => 'required',
      'email' => 'required',
      'distrito' => 'required',
      'provincia' => 'required',
      'departamento' => 'required',
      'distribuidor' => 'required',
      'motor' => 'required',
      'chasis' => 'required',
      'fechacompra' => 'required',
    ];

    protected $fillable = ['nombres','apellidos','num_documento','telefono','email','num_boleta'];
    protected $hidden = ['password', 'remember_token'];

    public function Historical() {
        return $this->hasMany('App\Models\Historical', 'users_id', 'id');
    }

}
