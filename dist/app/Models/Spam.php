<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class Spam extends Model{
    use Authenticatable;

    static $rules= [];
    protected $table = 'spams';
    protected $fillable = ['ip','codigo'];

}
