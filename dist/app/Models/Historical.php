<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Historical extends Model {
    protected $table = 'historicals';
    protected $fillable = ['users_id', 'codigo','kms','ip'];
    static $rules= [
      'users_id' => 'required',
   		'codigo' => 'required',
      'ip' => 'required'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }

}
