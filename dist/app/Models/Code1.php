<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class Code1 extends Model implements AuthenticatableContract{
    use Authenticatable;

    static $rules= [];
    protected $table = 'code1s';
    protected $fillable = ['codigo','estado','password'];

}
