<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*$app->get('/', function () use ($app) {
    return $app->welcome();
});*/

$app->get('/', 'PageController@index');
$app->get('gracias', 'PageController@gracias');

$app->post('/api/vcode', 'Api\CodeController@validateCode');
$app->post('/api/vusr', 'Api\CodeController@validateUser');
$app->post('/api/vusrp', 'Api\CodeController@validateUserxPass');
$app->post('/api/rusrp', 'Api\CodeController@updateSocio');
$app->post('/api/rcode', 'Api\CodeController@registrarUsuario');
$app->post('/api/vmotor', 'Api\CodeController@validateMotor');

$app->get('/ubigeo', 'UbigeoController@index');
$app->post('/provincias', 'UbigeoController@provincia');
$app->post('/distritos', 'UbigeoController@distrito');
////probar con JASON: http://localhost:8081/tampico/public/index.php/api/vcode
/*
{
"code":"24fo1s8ulp"
}
*/

$app->get('/reporte', 'UserController@index');
$app->get('/descargar', 'UserController@descargar');


$app->get('password/email', 'PasswordController@getEmail');
$app->post('password/email', 'PasswordController@postEmail');

// Password reset routes...
$app->get('password/reset/{token}', 'PasswordController@getReset');
$app->post('password/reset', 'PasswordController@postReset');
