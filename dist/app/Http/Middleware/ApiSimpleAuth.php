<?php

namespace App\Http\Middleware;

use Closure, Request, Response;

class ApiSimpleAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next){

       if (Request::getUser() != 'admin' || Request::getPassword() != 'pass4taic32qTY1'){
         $headers = array('WWW-Authenticate' => 'Basic');
         return response()->make('Invalid credentials.', 401, $headers);
         //return response()->json(['error' => 'Unauthenticated.'], 401);
       }

       return $next($request);

     }

}
