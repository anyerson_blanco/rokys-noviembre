<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Charts;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends BaseController
{

  var $detect;

  public function __construct()
  {
    $this->middleware('api.auth.basic');
  }


public function index()
 {
    $users = \App\User::where('estado', 'A')->orderBy('id','desc')->take(50)->get();

    $total= \App\User::where('estado', 'A')->count();;

    $chart = Charts::database(\App\User::all(), 'bar', 'highcharts')
    ->elementLabel("Total")
    ->dimensions(0, 400)
    ->responsive(false)
    ->lastByDay(65)
    ->backgroundColor('#e01f27');;

    return view('back.reporte', ['users' => $users, 'chart' => $chart, 'total' => $total]);
 }

 public function descargar(){
   Excel::create('Usuarios registrados', function($excel) {

           $excel->sheet('Usuarios', function($sheet) {

               $usuarios = \App\User::All();
               $sheet->fromArray($usuarios);

           });
       })->export('xlsx');
 }

}
