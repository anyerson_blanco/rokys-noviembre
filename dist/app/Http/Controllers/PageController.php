<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;


class PageController extends BaseController
{

  var $detect;

  public function __construct()
  {
    //echo(base_path(''))   ;   //C:\xampp\htdocs\tampico-airlines\server
    //include(base_path('')  . '/Mobile_Detect.php');
    //$this->detect  = new \Mobile_Detect;
  }

  public function gracias()
   {
      return view('front.gracias');

    }
public function index()
 {
    $rpta=$this->dias_restantes('2019-08-14');
    return view('front.index', ['dias' => $rpta]);

  }

  private function dias_restantes($fecha_final) {
    $fecha_actual = date("Y-m-d");
    $s = strtotime($fecha_final)-strtotime($fecha_actual);
    $d = intval($s/86400);
    $diferencia = $d;
    return $diferencia;
  }

}
