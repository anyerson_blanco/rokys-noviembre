<?php namespace App\Http\Controllers\Api;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadeRequest;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Models\Code as Code;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Config as Config;
use App\Models\Spam;
use DB;

class CodeController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	 public function index()
 	{


 	}

	 /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

  public function validateCode(Request $request)
	{
		//validar si no es un bot intentando validar códigos (15 minutos intentando agregar códigos falsos)
		$countIntentos=$this->validarSiEsSpam(FacadeRequest::ip());

		if($countIntentos>3){
			$data['estado'] = -1;
			die(json_encode($data));
		}
		//parseando el request json
		$objectModel=  (object) $request->json()->all();
		$code=$objectModel->code;
		$code=ltrim($code,'0');
		$numdocumento=$objectModel->documento;
    $credentials=["codigo" => $code,"password" => "hsjkd356El2klls"];

		$numcodigosParticipante = DB::table('historicals')->where('codigo', $code)->count();
		if($numcodigosParticipante>0){
			//si este codigo ya fue usado mandamos una respuesta con valor 3
			$data['token'] = false;
			$data['estado'] = "3";
			return $data;
		}

		//seleccionando el modelo Code como provider auth, se usara para validar si el codigo es correcto o no
    \Auth::setProvider(new \Illuminate\Auth\EloquentUserProvider(\App::make('hash'), '\App\Models\Code1'));

		//csrf (creando token de verificacion de la sesion)
		if ( ! $token = JWTAuth::attempt($credentials) )
		{
				//si el codigo no es correcto se agrega como spam
				$estado = 0;
				$spam= new Spam;
				$spam->ip = FacadeRequest::ip();
				$spam->codigo = $code;

				$spam->save();
		}else{
				//si es correcto se asigna una respuesta de estado 1
				$user = DB::table('users')->select('nombres', 'apellidos', 'num_documento', 'telefono', 'email')->where('num_documento', $numdocumento)->where('estado', 'A')->first();
				if($user === null){
					$data['user'] = '';
				}else{
					$data['user']=$user;
				}
				$estado = 1;
		}
		//retornando el token
		$data['token'] = $token;
		$data['estado'] = $estado;
    return $data;
	}


	public function validateUser(Request $request)
	{
		//parseando el request json
		$objectModel=  (object) $request->json()->all();
		$tipodocumento=$objectModel->tipodocumento;
		$numdocumento=$objectModel->numdocumento;
    $user = DB::table('users')->select('nombres', 'apellidos', 'num_documento', 'telefono', 'email')->where('num_documento', $numdocumento)->where('estado', 'A')->first();

		if($user === null){
			$data['estado'] = 0;
			$data['user'] = '';
		}else{
			$data['estado'] = 1;
			$data['user']=$user;
		}

    return $data;
	}


	public function validateUserxPass(Request $request)
	{
		//parseando el request json
		$objectModel=  (object) $request->json()->all();
		$pass=$objectModel->password;
		$numdocumento=$objectModel->numdocumento;

		\Auth::setProvider(new \Illuminate\Auth\EloquentUserProvider(\App::make('hash'), '\App\User'));

		$credentials=["num_documento" => $numdocumento,"password" => $pass];

		if ( ! $token = JWTAuth::attempt($credentials) )
		{
				$data['estado'] = 0;
				$data['user'] = '';
		}else{

				$user=\Auth::User();
				$numkms = DB::table('winners')->where('users_id', $user->id)->sum('kms');

				$data['estado'] = 1;
				$data['token'] = $token;
				$data['numkms'] = $numkms;
				$data['user']=$user;
				$estado = 1;
		}

    return $data;
	}

	public function validateMotor(Request $request){
		$objectModel=  (object) $request->json()->all();
		$numcodigosMotor = DB::table('users')->where('num_boleta', $objectModel->motor)->count();
		if( $numcodigosMotor > 0 ){
			//si este motor ya fue usado mandamos una respuesta con valor 3
			$data['rpta'] = "3";
			return $data;
		}
		$data['rpta'] = "1";
		return $data;
	}



	public function registrarUsuario(Request $request)
	{
		$objectModel=  (object) $request->json()->all();

		//validar si el codigo del motor ya participo anteriormente
		$numcodigosMotor = DB::table('users')->where('num_boleta', $objectModel->boleta)->count();
		if( $numcodigosMotor > 0 ){
			//Si esta boleta esta en uso devuelve 3
			$data['rpta'] = "3";
			return $data;
		}
		//validar si el usuario existe

		DB::beginTransaction();

		try {
			$idUser=DB::table('users')->insertGetId(
				['nombres' => $objectModel->nombres,
				'num_documento' => $objectModel->documento,
				'telefono' => $objectModel->telefono,
				'email' => $objectModel->email,
				'num_boleta' => $objectModel->boleta,
				'created_at' => DB::raw('now()')
				]
			);
		 	//se inserta en el historial
			$idHistorical=DB::table('historicals')->insertGetId(
					['users_id' => $idUser,
					'ip' => FacadeRequest::ip(),
					'created_at' => DB::raw('now()')
				]
			);

			$data['rpta'] = "1";

			DB::commit();
			return $data;

		} catch (ErrorException $e) {
			$data['rpta'] = "-1";

			DB::rollback();
			return $data;
		}

	}

	private function compararTokenCodigo($codigo){
		\Auth::setProvider(new \Illuminate\Auth\EloquentUserProvider(\App::make('hash'), '\App\Models\Code1'));
		//var_dump(JWTAuth::parseToken()->authenticate());
			try {
				if ($code = JWTAuth::parseToken()->authenticate()) {
					if ($codigo==$code->codigo) {
							return "1";
					}else{
							return response()->json(['rpta' => 'token_invalid']);
					}
				}
			} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
				return response()->json(['rpta' => 'token_expired']);
			} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
				return response()->json(['rpta' => 'token_invalid']);
			} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
				return response()->json(['rpta' => 'token_absent']);
			}catch (\Exception $e) {
				return response()->json(['rpta' => 'token_invalid']);
			}
	}

	private function compararTokenDocumento($numdocumento){
		\Auth::setProvider(new \Illuminate\Auth\EloquentUserProvider(\App::make('hash'), '\App\User'));
		//var_dump(JWTAuth::parseToken()->authenticate());
			try {
				if ($user = JWTAuth::parseToken()->authenticate()) {
					if ($numdocumento==$user->num_documento) {
							return $user->id;
					}else{
							return response()->json(['rpta' => 'token_invalid']);
					}
				}
			} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
				return response()->json(['rpta' => 'token_expired']);
			} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
				return response()->json(['rpta' => 'token_invalid']);
			} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
				return response()->json(['rpta' => 'token_absent']);
			}catch (\Exception $e) {
				return response()->json(['rpta' => 'token_invalid']);
			}
	}

	private function validarSiEsSpam($ip){
			$num = DB::table('spams')->where('ip', $ip)->where('created_at','>',DB::raw('DATE_SUB(NOW(), INTERVAL 15 MINUTE)'))->count();
			return $num;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
