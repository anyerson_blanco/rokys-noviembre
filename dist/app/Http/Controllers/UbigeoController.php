<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class UbigeoController extends BaseController
{

public function index()
 {
   $departamento = \App\Zone::groupBy('departamento')->select('departamento')->get();
   return $departamento;
 }

 public function provincia()
  {
    $departamento=$_POST['departamento'];
    $provincias = \App\Zone::where('departamento', $departamento)->groupBy('provincia')->select('provincia')->get();
    return $provincias;
  }


  public function distrito()
   {
     $distrito = \App\Zone::where('departamento', $_POST['departamento'])->where('provincia', $_POST['provincia'])->select('distrito')->get();
     return $distrito;
   }



}
