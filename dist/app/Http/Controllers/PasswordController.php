<?php

namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\ResetsPasswords;

use Illuminate\View\Factory;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\Http\Requests\Auth\EmailPasswordLinkRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;

class PasswordController extends BaseController
{
    use ResetsPasswords;

    public function __construct()
    {
       $this->broker = 'password';
    }


}
