<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();

        // $this->call('UserTableSeeder');

        //Model::reguard();

        $faker = Faker::create();

        for ($x = 1; $x <= 100; $x++):
           \App\Models\Code1::create(Array(
                  'codigo' => $faker->unique()->bothify('##??##????'),
                  'password' => '$2y$10$efSdXxumXIUJXvW5Nr9YYe3R9ubsG7.qCXALNHOrT/g/D9C/larlq',
                  'created_at' => '0000-00-00 00:00:00',
						      'updated_at' => '0000-00-00 00:00:00'
            ));
        endfor;

    }
}
