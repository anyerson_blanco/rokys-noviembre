<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">

    <div class="carousel-item ">
      <img src="image/fondo.jpg" width="460" height="345">
      <div class="logo-tvs">
        <img src="image/logo-tvs.png" >
      </div>
      <div class="el-mejor">
        <img src="image/el-mejor.png" >
      </div>
      <div class="gool">
        <img src="image/gool.png" >
      </div>
      <div class="participa">
        <img src="image/logo-tvs-king.png" >
        <img src="image/cartel-tele.png" >
        <a href="javascriopt:void(0)" class="right"><img class="mt-4" src="image/btn-participa.png" ></a>
      </div>
      <div class="footer">
        <div class="logo col-4 float-left">
          <img src="image/logo-tvs.jpg" >
        </div>
        <div class="col-4 float-left">
          <p>TÉRMINOS Y CONDICIONES | POLÍTICAS DE PRIVACIDAD</p>
        </div>
        <div class="copyright col-4 float-left">
          <small>TVS KING &#169;, TVS Todos los derechos reservados - By <a href="https://webtilia.com" class="footer-terminos__link" target="_blank">Webtilia</a></small>
        </div>
      </div>
    </div>

    <div class="carousel-item ">
      <img src="image/fondo.jpg" width="460" height="345">
      <div class="mecanica col-12 col-md-4">
        <img src="image/txt-mecanica.png" >
        <p class="texto1">Por la compra de una TVS King del 10 de mayo al 10 de junio, entras al sorteo de 10 TV de 43’’.</p>
        <p class="texto2">(05 televisores para Lima Metropolitana y 05 televisores para Provincia.)</p>
        <p class="texto3">Solo deberás ingresar a www.tvstumejorgol.com, completar el formulario y aceptar los términos y condiciones.</p>
        <p class="texto4">Oferta a nivel nacional con cualquier medio de pago.</p>
        <div class="inicio">
          <a href="javascriopt:void(0)" class="left"><img src="image/btn-inicio.png" > Inicio </a>
        </div>
      </div>
      <div class="datos col-12 col-md-4">
        <img class="cornetas" src="image/cornetas2.png" >
        <div class="caja">
          <div class="text-center mt-4 mb-4">
            <img src="image/tvsking-mundial-landing-form1_09.png" >
          </div>
          <form class="formulario">
            <div class="form-group">
              <select class="form-control" id="distriuidor" name="distriuidor" placeholder="Nombre del distriuidor donde compró">
                <option value="default">Nombre del distriuidor donde compró</option>
              </select>
            </div>

            <div class="form-group text-center">
              <div class="form-check form-check-inline mr-5">
                <span class="ckeck"></span>
                <input class="form-check-input" type="checkbox" name="gasolina" id="gasolina" value="option1">
                <label class="form-check-label" for="gasolina"> Gasolina </label>
              </div>
              <div class="form-check form-check-inline">
                <span class="ckeck"></span>
                <input class="form-check-input" type="checkbox" name="glp" id="glp" value="option2">
                <label class="form-check-label" for="glp"> GLP </label>
              </div>
            </div>

            <div class="form-group">
              <input class="form-control" type="text" name="motor" id="motor" placeholder="N° de Motor">
            </div>
            <div class="form-group">
              <input class="form-control" type="text" name="chasis" id="chasis" placeholder="N° de Chasis">
            </div>
            <div class="form-group">
              <input class="form-control" type="text" name="fecha" id="fecha" placeholder="Fecha de compra">
            </div>

            <div class="form-group text-center">
              <div class="form-check form-check-inline">
                <span class="ckeck"></span>
                <input class="form-check-input" type="checkbox" name="gasolina" id="gasolina" value="option1">
                <label class="form-check-label" for="gasolina"> Acepto los términos y condiciones </label>
              </div>
            </div>

            <div class="text-center">
              <a href="javascriopt:void(0)" class="right"><img src="image/btn-siguiente.png" ></a>
            </div>

          </form>
        </div>
      </div>
      <div class="footer">
        <div class="logo col-4 float-left">
          <img src="image/logo-tvs.jpg" >
        </div>
        <div class="col-4 float-left">
          <p>TÉRMINOS Y CONDICIONES | POLÍTICAS DE PRIVACIDAD</p>
        </div>
        <div class="copyright col-4 float-left">
          <small>TVS KING &#169;, TVS Todos los derechos reservados - By <a href="https://webtilia.com" class="footer-terminos__link" target="_blank">Webtilia</a></small>
        </div>
      </div>
    </div>

    <div class="carousel-item active">
      <img src="image/fondo.jpg" width="460" height="345">
      <div class="mecanica col-12 col-md-4">
        <img src="image/txt-mecanica.png" >
        <p class="texto1">Por la compra de una TVS King del 10 de mayo al 10 de junio, entras al sorteo de 10 TV de 43’’.</p>
        <p class="texto2">(05 televisores para Lima Metropolitana y 05 televisores para Provincia.)</p>
        <p class="texto3">Solo deberás ingresar a www.tvstumejorgol.com, completar el formulario y aceptar los términos y condiciones.</p>
        <p class="texto4">Oferta a nivel nacional con cualquier medio de pago.</p>
        <div class="inicio">
          <a href="javascriopt:void(0)" class="left"><img src="image/btn-inicio.png" > Inicio </a>
        </div>
      </div>
      <div class="datos2 col-12 col-md-4">
        <img class="cornetas" src="image/cornetas2.png" >
        <div class="caja">
          <div class="text-center mt-4 mb-4">
            <img src="image/tvsking-mundial-landing-form1_09.png" >
          </div>
          <form class="formulario">
            <div class="form-group">
              <input class="form-control" type="text" name="nombres" id="nombres" placeholder="Nombres">
            </div>
            <div class="form-group">
              <input class="form-control" type="text" name="apellidos" id="apellidos" placeholder="Apellidos">
            </div>
            <div class="form-group col-6 float-left pl-0">
              <input class="form-control" type="text" name="dni" id="dni" placeholder="DNI">
            </div>
            <div class="form-group col-6 float-left pr-0">
              <input class="form-control" type="text" name="celular" id="celular" placeholder="Celular/teléfono">
            </div>
            <div class="form-group">
              <input class="form-control" type="email" name="email" id="email" placeholder="Correo electrónico">
            </div>
            <div class="form-group">
              <select class="form-control" id="departamento" name="departamento">
                <option value="default">Departamento</option>
              </select>
            </div>
            <div class="form-group">
              <select class="form-control" id="provincia" name="provincia">
                <option value="default">Provincia</option>
              </select>
            </div>
            <div class="form-group">
              <select class="form-control" id="distrito" name="distrito">
                <option value="default">Distrito</option>
              </select>
            </div>

            <div class="text-center">
              <a href="javascriopt:void(0)" class="right"><img src="image/btn-anterior.png" ></a>
              <a href="javascriopt:void(0)" class="left"><img src="image/btn-enviar.png" ></a>
            </div>

          </form>
        </div>
      </div>
      <div class="footer">
        <div class="logo col-4 float-left">
          <img src="image/logo-tvs.jpg" >
        </div>
        <div class="col-4 float-left">
          <p>TÉRMINOS Y CONDICIONES | POLÍTICAS DE PRIVACIDAD</p>
        </div>
        <div class="copyright col-4 float-left">
          <small>TVS KING &#169;, TVS Todos los derechos reservados - By <a href="https://webtilia.com" class="footer-terminos__link" target="_blank">Webtilia</a></small>
        </div>
      </div>
    </div>

  </div>
</div>

<script>
$(document).ready(function(){
    // Activate Carousel
    $("#carouselExampleControls").carousel('pause');

    // Enable Carousel Controls
    $(".left").click(function(){
        $("#carouselExampleControls").carousel("prev");
    });
    $(".right").click(function(){
      $("#carouselExampleControls").carousel();
      $("#carouselExampleControls").carousel("next");
      $("#carouselExampleControls").carousel('pause');
    });

    // Activar checkboxs
    $(".form-check-input").click(function(){
      if($(this)[0].checked){
        $(this).siblings('span').addClass('activo');
      }else{
        $(this).siblings('span').removeClass('activo');
      }
    });

});
</script>

</body>
</html>
