  public function index()
  {
    $departamento = \App\Ubigeo::groupBy('departamento')->select('departamento')->get();
  }

  public function provincia()
  { 
    $provincia = \App\Ubigeo::where('departamento', $_POST['data'])->groupBy('provincia')->select('provincia')->get();
    return response()->json(array('provincia' => $provincia), 200);
  }

  public function distrito()
  { 
    $distrito = \App\Ubigeo::where('departamento', $_POST['departamento'])->where('provincia', $_POST['provincia'])->select('distrito')->get();
    return response()->json(array('distrito' => $distrito), 200);
  }