-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-11-2019 a las 09:04:36
-- Versión del servidor: 10.1.41-MariaDB-cll-lve
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rokysco_sorteo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cache`
--

CREATE TABLE `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicals`
--

CREATE TABLE `historicals` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `historicals`
--

INSERT INTO `historicals` (`id`, `users_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 919, '181.224.239.66', '2019-11-13 15:57:38', '0000-00-00 00:00:00'),
(2, 920, '181.224.239.66', '2019-11-13 16:04:35', '0000-00-00 00:00:00'),
(3, 921, '181.224.239.66', '2019-11-13 16:06:18', '0000-00-00 00:00:00'),
(4, 922, '181.224.239.66', '2019-11-13 16:07:16', '0000-00-00 00:00:00'),
(5, 923, '181.224.239.66', '2019-11-13 16:08:42', '0000-00-00 00:00:00'),
(6, 924, '190.236.15.248', '2019-11-13 16:08:45', '0000-00-00 00:00:00'),
(7, 925, '181.224.239.66', '2019-11-13 16:09:14', '0000-00-00 00:00:00'),
(8, 926, '181.224.239.66', '2019-11-13 16:33:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2017_08_23_211409_create_users_table', 1),
('2017_08_23_223946_create_historicals_table', 1),
('2017_08_24_222704_create_cache_table', 1),
('2017_08_26_043000_create_spams_table', 1),
('2017_09_01_204110_create_passwords_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `spams`
--

CREATE TABLE `spams` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombres` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `num_documento` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `num_boleta` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombres`, `num_documento`, `telefono`, `email`, `num_boleta`, `remember_token`, `created_at`, `updated_at`) VALUES
(919, 'anyerson', '12345678', '924501528', 'anyerson@gmail.com', '12345678', NULL, '2019-11-13 15:57:38', '0000-00-00 00:00:00'),
(920, 'catherine larrain peña', '46398018', '984138194', 'catherine.larrain@gmail.com', '123456789', NULL, '2019-11-13 16:04:35', '0000-00-00 00:00:00'),
(921, 'anyerson blanco', '12345678', '924525528', 'anyersonblanco@gmail.com', '123asd45', NULL, '2019-11-13 16:06:18', '0000-00-00 00:00:00'),
(922, 'gdfgdf ttgfdgdf', '45218795', '452166465', 'mdshf@gmail.com', '1', NULL, '2019-11-13 16:07:16', '0000-00-00 00:00:00'),
(923, 'ziu macayo', '12345786', '969606801', 'zmacayo@gmail.com', '1234567j', NULL, '2019-11-13 16:08:42', '0000-00-00 00:00:00'),
(924, 'Catherine Larrain Peña', '46398018', '984138194', 'catherine.larrain@gmail.com', '12345y78', NULL, '2019-11-13 16:08:45', '0000-00-00 00:00:00'),
(925, 'dfgdfgdfg', '21', '-14', 'vg@jkasdjw.com', '12345678942', NULL, '2019-11-13 16:09:14', '0000-00-00 00:00:00'),
(926, 'anyerson blanco', '12345678', '924501528', 'anyersonblancoj@gmail.com', 'asdasd123', NULL, '2019-11-13 16:33:08', '0000-00-00 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indices de la tabla `historicals`
--
ALTER TABLE `historicals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `historicals_users_id_foreign` (`users_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `spams`
--
ALTER TABLE `spams`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `historicals`
--
ALTER TABLE `historicals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `spams`
--
ALTER TABLE `spams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=927;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `historicals`
--
ALTER TABLE `historicals`
  ADD CONSTRAINT `historicals_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
