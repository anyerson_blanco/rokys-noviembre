var gulp = require('gulp')
var browserSync = require('browser-sync').create()
var reload = browserSync.reload
var sass = require('gulp-sass')
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

// Servidor de desarrollo
gulp.task('serve', function () {
  browserSync.init({
  /*server: {
      baseDir: './tampicoApp'
    },*/
    proxy: "http://localhost/rokys-noviembre/dist/public/"
  })
})

// Tarea para procesar el CSS
gulp.task('buildCss', function () {
  return gulp.src('./scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    //.pipe(cleanCSS())
    .pipe(gulp.dest('./dist/public/css'))
    .pipe(browserSync.stream())
})

// Tarea para vigilar los cambios
gulp.task('watch', function () {
  gulp.watch(['./scss/style.scss','./scss/**/*.scss'], ['buildCss'])
  //gulp.watch(['./scss/mobile.scss','./scss/**/*.scss'], ['cssMobile'])
  //gulp.watch(['./scss/password-reset.scss','./scss/**/*.scss'], ['cssPassword'])
  gulp.watch(['dist/public/**/*.html'], reload);
  gulp.watch(['dist/public/js/**/*.js'], reload);
  gulp.watch(['dist/public/img/**/*'], reload);
  gulp.watch(['dist/resources/views/**/*.php'], reload);
})

/*gulp.task('sass', function () {
  return gulp.src('./scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./tampicoApp/public/css/vendor'));
});*/

gulp.task('default', ['watch', 'serve'])
